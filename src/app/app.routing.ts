import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { SignupComponent } from './examples/signup/signup.component';
import { LandingComponent } from './examples/landing/landing.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import {RouteCreationComponent} from './examples/routeCreation/routeCreation.component';
import {RegistrationComponent} from './examples/registration/registration.component';
import {MessageComponent} from './examples/message/message.component';
import {SearchComponent} from './examples/search/search.component';
import {ProfileNewComponent} from './examples/profile-new/profile-new.component';
import {MapComponent} from './examples/map/map.component';
import {MonumentPageComponent} from './examples/monumentPage/monumentPage.component';
import {HotelPageComponent} from './examples/hotelPage/hotelPage.component';
import {WebsocketChatComponent} from "./examples/websocket-chat/websocket-chat.component";
import {UserProfileComponent} from "./examples/user-profile/user-profile.component";
import {EmailConfirmComponent} from "./examples/emailСonfirm/emailConfirm.component";
import {AfterRegistrationComponent} from "./examples/afterRegistration/afterRegistration.component";
import {UserFollowersComponent} from "./examples/user-followers/user-followers.component";
import {UserRoutesComponent} from "./examples/user-routes/user-routes.component";
import {ShowWayComponent} from './examples/showWay/showWay.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home',             component: ComponentsComponent },
    { path: 'user-profile',     component: ProfileComponent },
    { path: 'signup',           component: SignupComponent },
    { path: 'landing',          component: LandingComponent },
    { path: 'nucleoicons',      component: NucleoiconsComponent },
    { path: 'route-creation',   component: RouteCreationComponent},
    { path: 'registration',     component: RegistrationComponent},
    { path: 'messages',         component: MessageComponent},
    { path: 'search-page',      component: SearchComponent},
    { path: 'profile',          component: ProfileNewComponent},
    { path: 'monument-information/:id',   component: MonumentPageComponent},
    { path: 'hotel-information/:id',   component: HotelPageComponent},
    { path: 'chat', component: WebsocketChatComponent},
    { path: 'profile-user', component: UserProfileComponent},
    { path: 'profile-user/:id', component: UserProfileComponent},
    { path: 'email_confirm',     component: EmailConfirmComponent},
    { path: 'after_registration',     component: AfterRegistrationComponent},
    { path: 'subscriptions', component: UserFollowersComponent},
    { path: 'created-routes', component: UserRoutesComponent},
    { path: 'show-way/:id',         component: ShowWayComponent}
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
