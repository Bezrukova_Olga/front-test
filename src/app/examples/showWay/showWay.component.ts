import {Component, OnInit} from '@angular/core';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {MapComponent} from '../map/map.component';
import {Status} from '../dto/routeCreation.status';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {Trip} from '../dto/routeCreation.trip';
import {Hotel} from '../dto/routeForHotels/routeCreation.hotel';
import {PhotoHotel} from '../dto/routeForHotels/routeCreation.photoHotel';
import {Url} from '../dto/routeCreation.url';
import {PointTrip} from '../dto/routeCreation.pointTrip';
import {TicketParams} from '../dto/routeCreation.ticketParams';
import {RouteCreationService} from '../routeCreation/routeCreation.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Steps} from '../dto/routeCreation.steps';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {HotelPageComponent} from '../hotelPage/hotelPage.component';
import {SearchService} from '../search/search.service';
import {ShowWayService} from './showWay.service';
import {Routes} from '../user-profile/user-profile.routes';
import {MonumentPageComponent} from '../monumentPage/monumentPage.component';
import {MonumentPageService} from '../monumentPage/monumentPage.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {TripUser} from '../dto/tripUser';
import {CheckPointResponses} from '../dto/CheckPointResponses';

@Component({
    selector: 'app-show-way',
    templateUrl: './showWay.component.html',
    styleUrls: ['./showWay.component.scss'],
    providers: [ShowWayService]
})
export class ShowWayComponent implements OnInit {
    static placeId = '';
    latitude = 51.661535;
    longitude = 39.200287;
    focus;
    locationChosen = false;
    closeResult: string;
    date: { year: number, month: number };
    public isCollapsed4 = true;
    public isCollapsed5 = true;
    isCollapsed6 =true;
    public isCollapsed20 = true;
    cityOnMap: City = new City();
    cityArr: City[] = [];
    monument: Monument[]= [];
    monumentToListMarker: Monument[] = [];
    map: MapComponent;
    monumentToList: PhotoMonument[] = [];
    // tslint:disable-next-line:max-line-length
    symbols: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'W', 'X', 'Y', 'Z'];
    wayPoint = [];
    enterMonument: PhotoMonument;
    dir = undefined;
    optimizeArray = [];
    optimizeWayPoints = true;
    result: any;
    show = true;
    showAgm = false;
    cluster_styles = [
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 12
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 14
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 16
        }
    ];
    cluster_styles_hotel = [
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 12
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 14
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 16
        }
    ];
    travelMode = 'WALKING';
    routeDescription: Trip[] = [];
    // переменные для отелей
    // checkHotelStar: boolean[] = [false, false, false, false, false, false];
    value: number;
    enterHotel: Hotel;
    iconUrl1 = {
        url: './assets/Icons/hotel.png',
        scaledSize: {
            width: 40,
            height: 50
        }
    };
    order = [];
    monumentToListEx = [];
    // optimalHotel: PhotoHotel = null;

    tempTripCity: CheckPointResponses = new CheckPointResponses();

    iteration = 0;

    private subscription: Subscription;
    private id: string;
    public routes: TripUser = new TripUser();

    constructor(private httpService: ShowWayService, private activateRoute: ActivatedRoute, private modalService: NgbModal) {
        this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
        console.log(this.id);
    }
    ngOnInit(): void {
        if(this.id!=='') {
            this.httpService.getTrip(this.id).subscribe((data: TripUser) => this.routes = data, error => console.log('This is called when error occurs'), () => this.checkTempInit());
        }
    }


    statusesCheck() {
        // получаем отельки
        if (this.tempTripCity.hotel) {
            this.showAgm = true;
            this.wayPoint = [];
            for (let i = 0; i < this.tempTripCity.listMonumentEntities.length; i++) {
                // tslint:disable-next-line:max-line-length
                this.wayPoint.push({location: {lat: this.tempTripCity.listMonumentEntities[i].monument.latitude, lng: this.tempTripCity.listMonumentEntities[i].monument.longitude}, stopover: true});
            }
            console.log(this.wayPoint);
            this.getDirectionWithHotel();
        }
        // tslint:disable-next-line:one-line
        else {
            this.showAgm = true;
            console.log(this.tempTripCity.listMonumentEntities);
            this.wayPoint = [];
            for (let i = 1; i < this.tempTripCity.listMonumentEntities.length - 1; i++) {
                // tslint:disable-next-line:max-line-length
                this.wayPoint.push({location: {lat: this.tempTripCity.listMonumentEntities[i].monument.latitude, lng: this.tempTripCity.listMonumentEntities[i].monument.longitude}, stopover: true});
            }
            if (this.tempTripCity.listMonumentEntities.length > 1) {
                this.getDirection();
            } else {
                this.showAgm = false;
            }
        }
    }
    // возвращаем номер города в списке выбранных
    returnIterCity(str: string) {
        let j = 0;
        for (const temp of this.routes.checkPoinResponses) {
            // tslint:disable-next-line:curly
            if (str === temp.nameCity.nameCity)
                return j;
            // tslint:disable-next-line:curly
            else j++;
        }
    }
    // вывод города на карте
    onPaintLocationCity() {
        this.latitude = this.tempTripCity.nameCity.latitude;
        this.longitude = this.tempTripCity.nameCity.longitude;
        this.locationChosen = true;
    }
    // выбрана достопремечательность какая нибудь
    onTempTripCity(iter: number) {
        this.tempTripCity = null;
        this.tempTripCity = new CheckPointResponses();
        this.tempTripCity.nameCity = this.routes.checkPoinResponses[iter].nameCity;
        // console.log(this.tempTripCity.cityObject);
        this.tempTripCity = this.routes.checkPoinResponses[iter];
        this.onPaintLocationCity();
        this.show = true;
        this.showAgm = false;
        this.monumentToListMarker = [];
        this.statusesCheck();
    }

    checkURL(){
        if(this.tempTripCity.url!='') {
            return true;
        }
        else
            return  false;
    }
    checkHotel(){
        if(this.tempTripCity.hotel!=null) {
            return true;
        }
        else
            return  false;
    }

    checkTempInit() {
        console.log("gtybc");
        console.log(this.routes);
        this.tempTripCity = this.routes.checkPoinResponses[0];
        this.tempTripCity.nameCity =  this.routes.checkPoinResponses[0].nameCity;
        console.log(this.tempTripCity);
        this.latitude = this.tempTripCity.nameCity.latitude;
        this.longitude = this.tempTripCity.nameCity.longitude;
        this.locationChosen = true;
        this.statusesCheck();
    }

    onChoseLocation(event) {
        this.latitude = event.coords.lat;
        this.longitude = event.coords.lng;
        this.locationChosen = true;
    }
    onMouseOver(infoWindow, gm) {
        if (gm.lastOpen != null) {
            gm.lastOpen.close();
        }
        gm.lastOpen = infoWindow;
        infoWindow.open();
    }
    // страшная Олина функция, лень разбирать
    onResponse(event) {
        console.log(event);
        this.optimizeArray = [];
        this.routeDescription = [];
        this.order = event.routes[0].waypoint_order;
        for (let i = 0; i < this.tempTripCity.listMonumentEntities.length - 2; i++) {
            this.optimizeArray.push(this.wayPoint[this.order[i]]);
            this.monumentToListEx[i] = this.tempTripCity.listMonumentEntities[this.order[i] + 1];
        }
        for (let i = 1; i < this.tempTripCity.listMonumentEntities.length - 1; i++) {
            this.tempTripCity.listMonumentEntities[i] = this.monumentToListEx[i - 1];
        }
        if (this.tempTripCity.hotel) {
            for (let i = 0; i < event.routes[0].legs.length; i++) {
                this.routeDescription[i] = new Trip();
                if (i === 0 && this.tempTripCity.hotel) {
                    this.routeDescription[i].start = this.tempTripCity.hotel.nameHotel;
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.listMonumentEntities[i].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i].monument.nameMonument.length - 2);
                } else if (i === event.routes[0].legs.length - 1 && this.tempTripCity.hotel !== null) {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].start = this.tempTripCity.listMonumentEntities[i - 1].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i - 1].monument.nameMonument.length - 2);
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.hotel.nameHotel;
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].start = this.tempTripCity.listMonumentEntities[i - 1].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i - 1].monument.nameMonument.length - 2);
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.listMonumentEntities[i].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i].monument.nameMonument.length - 2);
                }
                this.routeDescription[i].distance = event.routes[0].legs[i].distance.value / 1000 + ' km ';
                const value = event.routes[0].legs[i].duration.value;
                if (Math.trunc(value / 3600) === 0) {
                    this.routeDescription[i].duration = Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].duration = Math.trunc(value / 3600) + ' h ' + Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                }
                for (let j = 0; j < event.routes[0].legs[i].steps.length; j++) {
                    const step = new Steps();
                    step.distance = event.routes[0].legs[i].steps[j].distance.value / 1000 + ' km ';
                    const valueSteps = event.routes[0].legs[i].steps[j].duration.value;
                    if (Math.trunc(valueSteps / 3600) === 0) {
                        step.duration = Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    } else {
                        // tslint:disable-next-line:max-line-length
                        step.duration = Math.trunc(valueSteps / 3600) + ' h ' + Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    }
                    step.maneuver = event.routes[0].legs[i].steps[j].maneuver;
                    this.routeDescription[i].steps.push(step);
                }
            }
            console.log(this.routeDescription);
        } else {
            for (let i = 0; i < event.routes[0].legs.length; i++) {
                this.routeDescription[i] = new Trip();
                // tslint:disable-next-line:max-line-length
                this.routeDescription[i].start = this.tempTripCity.listMonumentEntities[i].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i].monument.nameMonument.length - 2);
                // tslint:disable-next-line:max-line-length
                this.routeDescription[i].end = this.tempTripCity.listMonumentEntities[i + 1].monument.nameMonument.substr(1, this.tempTripCity.listMonumentEntities[i + 1].monument.nameMonument.length - 2);
                this.routeDescription[i].distance = event.routes[0].legs[i].distance.value / 1000 + ' km ';
                const value = event.routes[0].legs[i].duration.value;
                if (Math.trunc(value / 3600) === 0) {
                    this.routeDescription[i].duration = Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].duration = Math.trunc(value / 3600) + ' h ' + Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                }
                for (let j = 0; j < event.routes[0].legs[i].steps.length; j++) {
                    const step = new Steps();
                    step.distance = event.routes[0].legs[i].steps[j].distance.value / 1000 + ' km ';
                    const valueSteps = event.routes[0].legs[i].steps[j].duration.value;
                    if (Math.trunc(valueSteps / 3600) === 0) {
                        step.duration = Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    } else {
                        // tslint:disable-next-line:max-line-length
                        step.duration = Math.trunc(valueSteps / 3600) + ' h ' + Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    }
                    step.maneuver = event.routes[0].legs[i].steps[j].maneuver;
                    this.routeDescription[i].steps.push(step);
                }
            }
            console.log(this.routeDescription);
        }
    }
    // открытие блока достопремечательностей
    open(content, movie: PhotoMonument) {
        this.isCollapsed6 = true;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.enterMonument = movie;
    }
    // открытие блока отелей
    openHotel(content, movie: Hotel) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        console.log(this.enterHotel);
        this.enterHotel = movie;
    }
    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    // удаление достопремеч из списка желаемых
    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.tempTripCity.listMonumentEntities, event.previousIndex, event.currentIndex);
        this.wayPoint = [];
        for (let i = 1; i < this.tempTripCity.listMonumentEntities.length - 1; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.listMonumentEntities[i].monument.latitude, lng: this.tempTripCity.listMonumentEntities[i].monument.longitude}, stopover: true});
        }
        this.optimizeWayPoints = false;
        this.show = false;
        this.showAgm = true;
        this.getDirection();
    }
    // отрисовка достопремичательностей без отеля
    public getDirection() {
        this.dir = undefined;
        this.dir = {
            origin: {lat: this.tempTripCity.listMonumentEntities[0].monument.latitude, lng: this.tempTripCity.listMonumentEntities[0].monument.longitude},
            // tslint:disable-next-line:max-line-length
            destination: {
                lat: this.tempTripCity.listMonumentEntities[this.tempTripCity.listMonumentEntities.length - 1].monument.latitude,
                lng: this.tempTripCity.listMonumentEntities[this.tempTripCity.listMonumentEntities.length - 1].monument.longitude
            },
            points: this.wayPoint
        }
    }
    // отрисовка достопремичательностей с отелем
    public getDirectionWithHotel() {
        this.dir = undefined;
        this.dir = {
            origin: {lat: this.tempTripCity.hotel.latitude, lng: this.tempTripCity.hotel.longitude},
            // tslint:disable-next-line:max-line-length
            destination: {
                lat: this.tempTripCity.hotel.latitude,
                lng: this.tempTripCity.hotel.longitude
            },
            points: this.wayPoint
        }
    }
    // открытие новых страничек
    routing(placeId: string) {
        console.log(placeId);
        HotelPageComponent.placeId = placeId;
    }
    // добавление отеля в путь + вывод
    onConfirm(placeId: string) {
        console.log(this.tempTripCity.hotel);

        this.showAgm = true;
        this.wayPoint = [];
        for (let i = 0; i < this.tempTripCity.listMonumentEntities.length; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.listMonumentEntities[i].monument.latitude, lng: this.tempTripCity.listMonumentEntities[i].monument.longitude}, stopover: true});
        }
        console.log(this.wayPoint);
        this.getDirectionWithHotel();
    }
}
