import {Component, Input, OnInit} from '@angular/core';
import {AllPhotoHotel} from './dto/hotelPage.photo';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {MonumentPageService} from '../monumentPage/monumentPage.service';
import {ActivatedRoute} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {HotelPageResponse} from '../dto/HotelPageResponse';
import {HotelPageComment} from '../dto/HotelPageComment';
import {MonumentPageResponse} from '../dto/MonumentPageResponse';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-hotel',
    templateUrl: './hotelPage.component.html',
    styleUrls: ['./hotelPage.component.scss'],
    providers: [MonumentPageService]
})
export class HotelPageComponent implements OnInit {
    static placeId = '';
    photoHotel: AllPhotoHotel = {
        longitude: null,
        latitude: null,
        nameHotel: '',
        avgPrice: null,
        priceAvg: null,
        stars: null,
        rating: null,
        site: '',
        formattedAddress: '',
        formattedPhoneNumber: '',
        photo: []
    };
    @Input()
    responses: Array<any>;
    hotelComment: HotelPageComment;
    private hasBaseDropZoneOver = false;
    uploader: FileUploader;
    id: string;
    url: string[] = [];
    change = false;
    time: string[] = [];
    date: string[] = [];
    closeResult: string;
    selectItem: MonumentPageResponse;
    selectPhoto: string;
    show = false;
    comment = '';
    photoUser: string[];
    status: string;
    private subscription: Subscription;
    uploaderOptions: FileUploaderOptions = {
        url: 'https://api.cloudinary.com/v1_1/daqgh2ufv/image/upload',
        autoUpload: true,
        isHTML5: true,
        removeAfterUpload: false,
        headers: [
            {
                name: 'X-Requested-With',
                value: 'XMLHttpRequest'
            }
        ]
    };
    allComments: HotelPageResponse[] = [];
    allCommentsNew: HotelPageResponse[] = [];
    vehicles: Observable<Array<HotelPageResponse>>;
    photoYes = false;

    constructor(private httpService: MonumentPageService, private activateRoute: ActivatedRoute, private modalService: NgbModal) {
        this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
    }

    ngOnInit(): void {
        if (this.id !== '') {
            // tslint:disable-next-line:max-line-length
            this.httpService.getAllPhotosHotel(this.id).subscribe((data: AllPhotoHotel) => this.photoHotel = data, error => console.log('This is called when error occurs'), () => this.empty());
        }
        this.httpService.getCommentHotel(this.id).subscribe((data: HotelPageResponse[]) => this.allComments = data,
            error => console.log('This is called when error occurs'), () => this.userCheck());
        this.uploader = new FileUploader(this.uploaderOptions);

        this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
            form.append('upload_preset', 'ml_default');
            form.append('file', fileItem);
            fileItem.withCredentials = false;
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            const responsePath = JSON.parse(response);
            this.url.push(responsePath.url);
        };
    }

    empty() {
        this.show = true;
    }

    cost() {
        this.httpService.getCommentHotel(this.id).subscribe((data: HotelPageResponse[]) => this.allComments = data,
            error => console.log('This is called when error occurs'),
            () => this.userCheck());
    }

    userCheck() {
        const idTempUser = localStorage.getItem('id');
        for (let i = 0; i < this.allComments.length; i++) {
            if (this.allComments[i].idUser === idTempUser) {
                this.allComments[i].check = true;
            } else {
                this.allComments[i].check = false;
            }
            if (this.allComments[i].photoUser === '') {
                this.allComments[i].photoUser = 'https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375';
            }
        }
        this.updateTime();
    }

    open(content, movie: MonumentPageResponse) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.selectItem = movie;
    }

    openPhoto(content, movie: string) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.selectPhoto = movie;
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addCommentHotel() {
        this.comment = (<HTMLInputElement>document.getElementById('textAreaHotel')).value;
        this.httpService.addCommentHotel(localStorage.getItem('id').toString(), this.id, this.comment, this.url, new Date())
            .subscribe((data: HotelPageComment) => this.hotelComment = data,
                error => console.log('This is called when error occurs'),
                () => this.cost());
        (<HTMLInputElement>document.getElementById('textAreaHotel')).value = '';
        this.url = [];
    }

    updateTime() {
        for (let i = 0; i < this.allComments.length; i++) {
            if (this.allComments[i].date !== null) {
                this.time[i] = new Date(this.allComments[i].date).toLocaleTimeString();
                this.date[i] = new Date(this.allComments[i].date).toLocaleDateString();
            } else {
                this.time[i] = '';
                this.date[i] = '';
            }
        }
    }

    deletePhoto(item: string) {
        this.url.splice(this.url.indexOf(item), 1);
    }

    deleteComment(content) {
        if (this.selectItem.idUser === localStorage.getItem('id')) {
            this.httpService.deleteCommentHotel(this.selectItem.idComment).subscribe((data: string) => this.status = data);
        }
        this.allComments.splice(this.allComments.indexOf(this.selectItem), 1);
        this.modalService.dismissAll();
    }
}
