import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AllPhotoHotel} from './dto/hotelPage.photo';

@Injectable()
export class HotelPageService {
    constructor(private http: HttpClient) {}

    getAllPhotos(placeId: string) {
        return this.http.get<AllPhotoHotel>('http://localhost:8080/hotel/photos/' + placeId);
    }

}