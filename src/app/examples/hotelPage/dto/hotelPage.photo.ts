import {WorkTimeMonument} from '../../dto/routeCreation.workTimeMonument';

export class AllPhotoHotel {
    longitude: number;
    latitude: number;
    nameHotel: string;
    avgPrice: number;
    priceAvg: number;
    stars: bigint;
    rating: number;
    site: string;
    formattedAddress: string;
    formattedPhoneNumber: string;
    photo: string[];
}
