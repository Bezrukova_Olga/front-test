import {Component, OnInit} from '@angular/core';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {Country} from '../dto/routeCreation.country';
import {RouteCreationService} from '../routeCreation/routeCreation.service';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    providers: [RouteCreationService]
})
export class MapComponent implements OnInit {
    latitude = 51.661535;
    longitude = 39.200287;
    locationChosen = false;
    country: Country[] = [];
    city: City[] = [];
    monument: Monument[] = [];
    countryTitle = 'Выберите страну';
    cityObject: City;
    monumentObject: Monument;
    constructor(private httpService: RouteCreationService) {}

    ngOnInit() {
    }

    onChoseLocation(event) {
        this.latitude = event.coords.lat;
        this.longitude = event.coords.lng;
        this.locationChosen = true;
    }
    onChange() {
        console.log(this.countryTitle);
        this.httpService.sendCountry(this.countryTitle).subscribe((newList: City[]) => this.city = newList);
    }

    onPaintLocationCity() {
        this.latitude = this.cityObject.latitude;
        this.longitude = this.cityObject.longitude;
        this.locationChosen = true;
    }

    onChangeCity() {
        this.onPaintLocationCity();
        this.httpService.sendCity(this.cityObject.nameCity).subscribe((newList: Monument[]) => this.monument = newList);
    }

    onPaintLocation() {
        this.latitude = this.monumentObject.latitude;
        this.longitude = this.monumentObject.longitude;
        this.locationChosen = true;
        console.log(this.monumentObject);
    }
}
