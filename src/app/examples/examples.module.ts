import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';

import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { SignupComponent } from './signup/signup.component';
import {RouteCreationComponent} from './routeCreation/routeCreation.component';
import {RegistrationComponent} from './registration/registration.component';
import {RouterModule} from '@angular/router';

import {BrowserModule} from '@angular/platform-browser';
import {MapComponent} from './map/map.component';
import {NouisliderModule} from 'ng2-nouislider';
import {HttpClientModule} from '@angular/common/http';
import {MessageComponent} from './message/message.component';
import {SearchComponent} from './search/search.component';
import {ProfileNewComponent} from './profile-new/profile-new.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {AgmDirectionModule} from 'agm-direction';
import {MonumentPageComponent} from './monumentPage/monumentPage.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import {HotelPageComponent} from './hotelPage/hotelPage.component';
import {WebsocketChatComponent} from './websocket-chat/websocket-chat.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as  Cloudinary from 'cloudinary-core';
import {FileUploadModule} from 'ng2-file-upload';
import {EmailConfirmComponent} from "./emailСonfirm/emailConfirm.component";
import {AfterRegistrationComponent} from "./afterRegistration/afterRegistration.component";
import {ShowWayComponent} from './showWay/showWay.component';
import {UserFollowersComponent} from "./user-followers/user-followers.component";
import {UserRoutesComponent} from "./user-routes/user-routes.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        RouterModule,
        AgmCoreModule,
        DragDropModule,
        FileUploadModule,
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do',
            libraries: ['places', 'geometry']
        }),
        NouisliderModule,
        CloudinaryModule.forRoot(Cloudinary, {
            cloud_name: 'daqgh2ufv',
            upload_preset: 'ml_default',
            cname: 'daqgh2ufv.images.com',
            api_key: '184833436448368',
            api_secret: 'cuwVLCgpMkbEBnMs6eUPEQ9QGvs'
        }),
        AgmJsMarkerClustererModule,
        AgmDirectionModule,
        FileUploadModule
    ],
    declarations: [
        LandingComponent,
        SignupComponent,
        ProfileComponent,
        RouteCreationComponent,
        RegistrationComponent,
        MapComponent,
        MessageComponent,
        SearchComponent,
        ProfileNewComponent,
        WebsocketChatComponent,
        UserProfileComponent,
        MonumentPageComponent,
        HotelPageComponent,
        EmailConfirmComponent,
        ShowWayComponent,
        AfterRegistrationComponent,
        UserFollowersComponent,
        UserRoutesComponent
    ],
    bootstrap:    [ RouteCreationComponent, MonumentPageComponent ]
})
export class ExamplesModule { }
