import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-registration',
    templateUrl: './afterRegistration.component.html',
    styleUrls: ['./afterRegistration.component.scss']
})

export class AfterRegistrationComponent implements OnInit {
    email: string;
    constructor(
        private route: ActivatedRoute
    ) {
    }
    ngOnInit() {
        this.route.queryParams.subscribe(
            (queryParam: any) => {
                this.email = queryParam['email'];
            }
        );
    }
}
