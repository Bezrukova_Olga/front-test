import {City} from './routeCreation.city';
import {PhotoHotel} from './routeForHotels/routeCreation.photoHotel';
import {PhotoMonument} from './routeCreation.photoMonument';
import {Status} from './routeCreation.status';
import {Country} from './routeCreation.country';
import {Url} from './routeCreation.url';

export class PointTrip {
    public cityObject: City;
    public optimalHotel: PhotoHotel;
    public monumentToList: PhotoMonument[] = [];
    public checkHotelStar: boolean[] = [false, false, false, false, false, false];
    public doubleSliderPriceHotel = [0, 5000];
    public doubleSliderRating = [0, 5];
    public tempCountry: string;
    // public urlTicket: Url = new Url();
    public urlTicket: string = '';
    constructor() {
    }

}
