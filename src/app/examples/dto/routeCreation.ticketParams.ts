
export class TicketParams {
    urlTicket: string;
    dateDeparture: Date;
    dateArrival: Date;
    cityDeparture: string;
    cityArrival: string;

}
