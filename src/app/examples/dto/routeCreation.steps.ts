export class Steps {
    private _distance: string;
    private _duration: string;
    private _maneuver: string;

    get distance(): string {
        return this._distance;
    }

    set distance(value: string) {
        this._distance = value;
    }

    get duration(): string {
        return this._duration;
    }

    set duration(value: string) {
        this._duration = value;
    }

    get maneuver(): string {
        return this._maneuver;
    }

    set maneuver(value: string) {
        this._maneuver = value;
    }
}
