export class User {
    id: string;
    login: string;
    nickname: string;
    role: string;
    photo: string;
}
