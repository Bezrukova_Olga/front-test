export class Hotel {
    longitude: number;
    latitude: number;
    nameHotel: string;
    priceAvg: number;
    avgPrice: number;
    stars: bigint;
    rating: number;
    site: string;
    formattedAddress: string;
    formattedPhoneNumber: string;
    placeId: string;
    photo: string;
}
