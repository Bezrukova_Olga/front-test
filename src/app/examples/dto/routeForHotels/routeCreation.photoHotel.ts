export class PhotoHotel {
    longitude: number;
    latitude: number;
    nameHotel: string;
    avgPrice: number;
    priceAvg: number;
    stars: bigint;
    rating: number;
    site: string;
    formattedAddress: string;
    formattedPhoneNumber: string;
    photo: string;
    check: Boolean = false;
    placeId: string;
}
