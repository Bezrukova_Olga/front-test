import {WorkTimeMonument} from './routeCreation.workTimeMonument';

export class PhotoMonument {
    nameMonument: string;
    photo: string;
    placeId: string;
    monumentInfo: string;
    latitude: number;
    longitude: number;
    workTimeMonument: WorkTimeMonument[];
    checked: boolean;
    nameStatus: string;
}
