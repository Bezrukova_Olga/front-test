export class MonumentPageComment {
    idUser: string;
    idMonument: string;
    comment: string;
    photo: string[];
    date: Date;
}
