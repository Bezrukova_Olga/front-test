import {Time} from '@angular/common';

export class WorkTimeMonument {
    day: string;
    timeOpen: Time;
    timeClose: Time;
}
