import {Hotel} from './routeForHotels/routeCreation.hotel';
import {TripMonumentResponse} from './routeCreation.tripMonumentResponse';
import {City} from './routeCreation.city';

export class CheckPointResponses {
    hotel: Hotel = new Hotel();
    listMonumentEntities: TripMonumentResponse[] = [];
    url: string;
    public nameCity: City = new City();
    public status: boolean;
}
