export class Status {
    nameStatus: string;
    link: string;
    checked: boolean;
}
