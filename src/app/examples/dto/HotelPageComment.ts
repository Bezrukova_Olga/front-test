export class HotelPageComment {
    idUser: string;
    idHotel: string;
    comment: string;
    photo: string[];
    date: Date;
}
