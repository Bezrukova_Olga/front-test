import {PhotoHotel} from './routeForHotels/routeCreation.photoHotel';
import {PhotoMonument} from './routeCreation.photoMonument';

export class TripOneCheckPoint {
    url: string ='';
    public hotel: PhotoHotel;
    public listMonument: PhotoMonument[] = [];
    public stars: number;
}
