import {Component, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Routes} from "../user-profile/user-profile.routes";

@Component({
    selector: 'app-created-routes',
    templateUrl: './user-routes.component.html',
    styleUrls: ['./user-routes.component.scss'],
})

export class UserRoutesComponent implements OnInit{
    idOf: string;
    routes: Routes[] = [];
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };
    constructor(private http: HttpClient) {
    }

    ngOnInit(): void {
        this.idOf = location.href.substring(location.href.indexOf('?id=') + 4, location.href.length);
        this.getRoutes();
    }

    getRoutes() {
        this.http.get('http://localhost:8080/user/routes/?id=' + this.idOf, this.httpOptions).subscribe((data: Routes[]) => {this.routes = data, console.log(data)});
    }
}