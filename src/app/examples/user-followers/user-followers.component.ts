import {Component, OnInit} from '@angular/core';
import {Sub} from './sub';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Issub} from '../user-profile/issub';

@Component({
    selector: 'app-followers',
    templateUrl: './user-followers.component.html',
    styleUrls: ['./user-followers.component.scss'],
})
export class UserFollowersComponent implements OnInit {
    subs: Sub[] = [];
    constructor(private http: HttpClient) {
    }
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };

    ngOnInit(): void {
        this.getSubs();
    }

    unsub(id: string) {
        this.http.delete('http://localhost:8080/user/unsub/?id_cur=' + localStorage.getItem('id') +
            '&id_from=' + id, this.httpOptions
        ).subscribe(
            (data: Issub) => {
                location.reload();
            }
        )
    }

    getSubs() {
        let idOf = location.href.substring(location.href.indexOf('?id=') + 4, location.href.length);
        this.http.get('http://localhost:8080/user/followers/?id=' + idOf, this.httpOptions).subscribe((data: Sub[]) =>
            {
                this.subs = data;
                console.log(data)
            }
        );
    }

    viewProfile(id: string) {
        location.replace('/#/profile-user?id=' + id);
    }
}
