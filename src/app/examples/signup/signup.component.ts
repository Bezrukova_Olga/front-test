import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiResponse} from "../dto/apiResponse";


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})

export class SignupComponent implements OnInit {
    test : Date = new Date();
    focus;
    focus1;
    data: ApiResponse;
    registerForm: FormGroup;
    submitted = false;

    constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            login: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]]
        });
    }

    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        const url = 'http://localhost:8080/login';
        const headers = new HttpHeaders()
            .set('login', this.f.login.value)
            .set('password', this.f.password.value);
        this.http.get(url, {headers: headers})
            .subscribe((data: ApiResponse)  => {
                this.data = data;
                if (this.data.status !== 424) {
                    if (this.data.status === 200) {
                        localStorage.setItem('id', this.data.userInfo.id);
                        localStorage.setItem('token', this.data.userInfo.token);
                        location.replace('http://localhost:4200/#/home?logged=1');
                    } else {
                        alert(this.data.message);
                    }
                } else {
                    alert(this.data.message);
                }
            });
    }
}
