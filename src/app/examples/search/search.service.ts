import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {Status} from '../dto/routeCreation.status';
import {User} from '../dto/routeCreation.user';
import {Observable} from 'rxjs';

@Injectable()
export class SearchService {
    constructor(private http: HttpClient) {
    }

    getMonumentsByCity(city: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/search/monuments/' + city);
    }

    getCountries() {
        return this.http.get<Country[]>('http://localhost:8080/countries');
    }

    getMonumentsByCountry(name: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/countries/' + name);
    }

    sendCountry(country: string) {
        return this.http.get<City[]>('http://localhost:8080/cities/' + country);
    }

    getCity() {
        return this.http.get<City[]>('http://localhost:8080/cities');
    }

    sendCity(city: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/monuments/' + city);
    }

    getUser(name: string) {
        return this.http.get<User>('http://localhost:8080/search/user/' + name);
    }

    getByNameMonument(name: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/search/monument/' + name)
    }
    filterMonuments(typeMonument: Status[]) {
        let url = '';
        for (let i = 0; i < typeMonument.length - 1; i++) {
            url = url + typeMonument[i].nameStatus + ',';
        }
        url = url + typeMonument[typeMonument.length - 1].nameStatus;
        return this.http.get<PhotoMonument[]>('http://localhost:8080/monuments/types?types=' + url);
    }

    getStatus() {
        return this.http.get<Status[]>('http://localhost:8080/statuses');
    }
}
