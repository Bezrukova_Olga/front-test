import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { WebsocketChatComponent } from './websocket-chat.component';
import {WebsocketChatMessage} from './websocket-chat.message';
import {Injectable} from '@angular/core';
import {Message} from "@angular/compiler/src/i18n/i18n_ast";

export class WebSocketAPI {
    webSocketEndPoint = 'http://localhost:8080/ws';
    topic = '/topic/greetings';
    stompClient: any;
    chatComponent: WebsocketChatComponent;
    messag: WebsocketChatMessage;
    constructor(chatComponent: WebsocketChatComponent) {
        this.chatComponent = chatComponent;
    }
    _connect() {
        console.log('Initialize WebSocket Connection');
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe(_this.topic, function (sdkEvent) {
                _this.onMessageReceived(sdkEvent);
            });
            //_this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log('Disconnected');
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error) {
        console.log('errorCallBack -> ' + error)
        setTimeout(() => {
            this._connect();
        }, 5000);
    }

    /**
     * Send message to sever via web socket
     * @param {*} message
     */
    _send(message: WebsocketChatMessage) {
        console.log('calling logout api via web socket ' + JSON.stringify(message));
        this.stompClient.send('/app/chating', {}, JSON.stringify(message));

    }

    onMessageReceived(message) {
        console.log('Message Recieved from Server :: ' + message);
        this.chatComponent.handleMessage(JSON.stringify(message.body));
        this.messag = JSON.parse(message.body);
        this.chatComponent.messages.push(this.messag);
    }
}