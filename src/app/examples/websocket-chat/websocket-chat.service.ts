import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class WebsocketChatService {
    httpOptions = {
        headers: new HttpHeaders({
            'Id':  localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    }
    constructor(public http: HttpClient) {}

    messages(sender: string, receiver: string) {
        return this.http.get('http://localhost:8080/messages/?sender=' + sender + '&recent=' + receiver, this.httpOptions);
    }
    dialogs(id: string) {
        return this.http.get('http://localhost:8080/messages/dialogs/?id=' + id), this.httpOptions;
    }
}
