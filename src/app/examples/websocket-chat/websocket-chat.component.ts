import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../../app.component';
import {WebSocketAPI} from './WebSocketApi';
import {WebsocketChatMessage} from './websocket-chat.message';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Dialog} from './dialog';
import {WebsocketChatService} from './websocket-chat.service';
import {createLogErrorHandler} from '@angular/compiler-cli/ngcc/src/execution/tasks/completion';


@Component({
    selector: 'app-websocket-chat',
    templateUrl: './websocket-chat.component.html',
    styleUrls: ['./websocket-chat.component.scss'],
})
export class WebsocketChatComponent implements OnInit {
    storage: Storage;
    greeting: any;
    name: string;
    recipient: string;
    previous = '';
    dialogs: Dialog[];
    currentDiag: number;
    webSocketAPI: WebSocketAPI;
    message: WebsocketChatMessage;
    messages: WebsocketChatMessage[];
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    }

    constructor(private http: HttpClient) {
    }

    ngOnInit() {

        this.currentDiag = 0;
        this.webSocketAPI = new WebSocketAPI(this);
        this.storage = window.localStorage;
        this.message = new WebsocketChatMessage();
        this.connect();
        if (location.href.includes('?hello=')) {
            console.log('hello')
            this.message.date = Date.now();
            this.message.content = 'hello';
            this.message.sender = localStorage.getItem('id')
            this.message.recipient = location.href.substring(location.href.indexOf('?hello=') + 7, location.href.length);
            setTimeout(() => {
                this.webSocketAPI._send(this.message)
            }, 1000);
            this.messages.push(this.message);
            let i: number;
            i = 0;
            for (i; i < this.dialogs.length; i++) {
                if (this.dialogs[i].senderId === this.message.recipient) {
                    break;
                }
                if (i === (this.dialogs.length - 1)) {
                    // tslint:disable-next-line:prefer-const
                    let dig: Dialog;
                    dig.senderId = this.message.sender;
                    dig.recevierId = this.message.recipient;
                    dig.lastmsg = this.message.content;
                    this.dialogs.push(dig);
                }
            }
            location.href = 'http://localhost:4200/#/chat'
            this.webSocketAPI._send(this.message)
        }
        console.log(this.storage.getItem('id'))
        this.getDialogs();
    }

    changeDiag(num: number) {
        if (this.previous !== '') {
            document.getElementById(this.previous).style.backgroundColor = '#ffffff';
            document.getElementById(this.previous).style.textDecorationColor = '#F5593D';
        }
        this.previous = num.toString();
        document.getElementById(num.toString()).style.backgroundColor = '#F5593D';
        document.getElementById(this.previous).style.textDecorationColor = '#ffffff';
        this.currentDiag = num;
        // tslint:disable-next-line:max-line-length
        this.http.get('http://localhost:8080/messages/?sender=' + this.storage.getItem('id') + '&recent=' + (localStorage.getItem('id') !== this.dialogs[this.currentDiag].recevierId ? this.dialogs[this.currentDiag].recevierId : this.dialogs[this.currentDiag].senderId), this.httpOptions)
            .subscribe((data: WebsocketChatMessage []) => this.messages = data)
    }


    getMessage(receiver: string) {
        this.http.get('http://localhost:8080/messages/?sender=' + this.storage.getItem('id') + '&recent=' + receiver, this.httpOptions)
            .subscribe((data: WebsocketChatMessage []) => this.messages = data)
    }


    getDialogs() {
        this.http.get('http://localhost:8080/messages/dialogs/?id=' + this.storage.getItem('id'), this.httpOptions)
            .subscribe((data: Dialog[]) => {
                this.dialogs = data;
                this.getMessage(this.dialogs[0].recevierId);
                // this.recipient = this.dialogs[0].recevierId;
                // this.message.recipient = this.recipient
                // console.log('complete')
            })

    }

    connect() {
        this.webSocketAPI._connect();
    }

    disconnect() {
        this.webSocketAPI._disconnect();
    }

    sendMessage() {
        this.message.date = Date.now();
        this.message.content = (<HTMLInputElement>document.getElementById('content')).value;
        this.message.sender = localStorage.getItem('id')
        // tslint:disable-next-line:max-line-length
        this.message.recipient = (localStorage.getItem('id') !== this.dialogs[this.currentDiag].recevierId ? this.dialogs[this.currentDiag].recevierId : this.dialogs[this.currentDiag].senderId);
        this.webSocketAPI._send(this.message);
        (<HTMLInputElement>document.getElementById('content')).value = '';
    }

    handleMessage(message) {
        this.greeting = message;
    }

}
