import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {Status} from '../dto/routeCreation.status';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {PhotoHotel} from '../dto/routeForHotels/routeCreation.photoHotel';
import {Hotel} from '../dto/routeForHotels/routeCreation.hotel';
import {Iata} from '../dto/routeCreation.iata';
import {Place} from '../dto/profile-new.placeCreation';
import {TripOneCheckPoint} from '../dto/routeCreation.tripOneCheckPoint';

@Injectable()
export class RouteCreationService {
    constructor(private http: HttpClient) {}

    getCountries() {
        // httpOptions.headers = httpOptions.headers.set('Authorization', 'my-new-auth-token');
        return this.http.get<Country[]>('http://localhost:8080/countries');
    }
    sendCountry(country: string) {
        return this.http.get<City[]>('http://localhost:8080/cities/' + country);
    }

    sendCity(city: string) {
        return this.http.get<Monument[]>('http://localhost:8080/monuments/' + city);
    }

    filterMonuments(city: string, typeMonument: Status[]) {
        let url = '';
        for (let i = 0; i < typeMonument.length - 1; i++) {
            url = url + typeMonument[i].nameStatus + ',';
        }
        url = url + typeMonument[typeMonument.length - 1].nameStatus;
        return this.http.get<Monument[]>('http://localhost:8080/monuments/' + city + '/types?types=' + url);
    }

    getStatus() {
        return this.http.get<Status[]>('http://localhost:8080/statuses');
    }

    getWishByCity(cityName: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/wish/search/city?cityName=' + cityName + '&idUser=' + localStorage.getItem('id') );
    }

    getPhotos(monument: Monument[]) {
        let url = '';
        for (let i = 0; i < monument.length - 1; i++) {
            if (monument[i].placeId !== null) {
                url = url + monument[i].placeId + ',';
            } else {
                url = url + monument[i].nameMonument + ',';
            }
        }
        if (monument[monument.length - 1].placeId !== null) {
            url = url + monument[monument.length - 1].placeId;
        } else {
            url = url + monument[monument.length - 1].nameMonument;
        }
        console.log(url);
        return this.http.get<PhotoMonument[]>('http://localhost:8080/photos?monuments=' + url);
    }
    // tslint:disable-next-line:max-line-length
    getHotelFilter(cityHotel: string, minPriceHotel: number, maxPriceHotel: number, minRatingHotel: number, maxRatingHotel: number,
                   star: boolean[]) {
        let temp: number;
        // tslint:disable-next-line:prefer-const
        let result: number;
        // @ts-ignore
        for (temp = star.length - 1, result = 1; temp >= 0 ; temp--) {
            // tslint:disable-next-line:curly triple-equals
            if (star[temp] == true)
                result = result * 10 + 1;
            // tslint:disable-next-line:curly
            else result = result * 10;
        }
        console.log(result);
        // tslint:disable-next-line:max-line-length
        const body = {city: cityHotel, minPrice: minPriceHotel, maxPrice: maxPriceHotel, minRating: minRatingHotel, maxRating: maxRatingHotel,
            star: result};
        const url = 'http://localhost:8080/hotels/filtered/';

        return this.http.post<Hotel[]>(url, body);
    }
    getPhotosHotel(city: string) {
        return this.http.get<PhotoHotel[]>('http://localhost:8080/photo/hotel/' + city);
    }
    getPhotosHotels(hotels: Hotel[]) {
        const body = {hotels: hotels};
        // const url = 'http://localhost:8080/hotels/filtered/';
        // return this.http.post<PhotoHotel[]>(url, body);
        return this.http.post<PhotoHotel[]>('http://localhost:8080/hotel/photo/id', body);
    }

    getIata(city: string) {
        return this.http.get<Iata>('http://localhost:8080/tickets/iata/?name=' + city);
    }

    getUrlAvia(dep: string, arr: string, fr: string, too: string) {
        return this.http.get('http://localhost:8080/tickets/aviaUrl/?placeFrom=' +
            dep + '&placeTo=' + arr + '&timeFrom=' + fr + '&timeTo=' + too);
    }
    getUrlTicket(url1: string) {
        let url = '';
        for(const st of url1) {
            if (st === '/')
                url = url + '!';
            else url = url + st;
        }
        return this.http.get('http://localhost:8080/tickets/params/' + url);
    }

    sendInfo(place: Place, photo: string) {
        const body = {
            name: place.name,
            longitude: place.longitude,
            latitude: place.latitude,
            description: place.description,
            category: place.category,
            country: place.country,
            city: place.city,
            photo: photo
        }
        return this.http.post('http://localhost:8080/place', body);
    }
    sendResultTrip(allTrip: TripOneCheckPoint[], user: string, nameTrip: string) {
        const body = {allTrip: allTrip, user: user, nameTrip: nameTrip};
        console.log(body);
        return this.http.post('http://localhost:8080/trip', body);
    }

    getHotelFilterWithPoin(cityHotel: string, minPriceHotel: number, maxPriceHotel: number, minRatingHotel: number, maxRatingHotel: number,
                           star: boolean[],  lon: number, lot: number) {
        let temp: number;
        // tslint:disable-next-line:prefer-const
        let result: number;
        // @ts-ignore
        for (temp = star.length - 1, result = 1; temp >= 0 ; temp--) {
            // tslint:disable-next-line:curly triple-equals
            if (star[temp] == true)
                result = result * 10 + 1;
            // tslint:disable-next-line:curly
            else result = result * 10;
        }
        console.log(result);
        // tslint:disable-next-line:max-line-length
        const body = {city: cityHotel, minPrice: minPriceHotel, maxPrice: maxPriceHotel, minRating: minRatingHotel, maxRating: maxRatingHotel,
            star: result, lat: lot, lon: lon};
        const url = 'http://localhost:8080/hotels/good/';

        return this.http.post<Hotel[]>(url, body);
    }
}
