import {} from 'google-maps';
import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RouteCreationService} from './routeCreation.service';
import {Country} from '../dto/routeCreation.country';
import {City} from '../dto/routeCreation.city';
import {Monument} from '../dto/routeCreation.monument';
import {MapComponent} from '../map/map.component';
import {Status} from '../dto/routeCreation.status';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AgmCoreModule, MapsAPILoader} from '@agm/core';
import DirectionsService = google.maps.DirectionsService;
import {Hotel} from '../dto/routeForHotels/routeCreation.hotel';
import {PhotoHotel} from '../dto/routeForHotels/routeCreation.photoHotel';
import {Trip} from '../dto/routeCreation.trip';
import {Steps} from '../dto/routeCreation.steps';
import * as jsPDF from 'jspdf';
import {Url} from '../dto/routeCreation.url';
import {Iata} from '../dto/routeCreation.iata';
import {MonumentPageComponent} from '../monumentPage/monumentPage.component';
import {HotelPageComponent} from '../hotelPage/hotelPage.component';
import {PointTrip} from '../dto/routeCreation.pointTrip';
import {placeholdersToParams} from '@angular/compiler/src/render3/view/i18n/util';
import {TicketParams} from '../dto/routeCreation.ticketParams';
import {TripOneCheckPoint} from '../dto/routeCreation.tripOneCheckPoint';
import {UserProfileComponent} from '../user-profile/user-profile.component';

@Component({
    selector: 'app-route',
    templateUrl: './routeCreation.component.html',
    styleUrls: ['./routeCreation.component.scss'],
    providers: [RouteCreationService]
})

export class RouteCreationComponent implements OnInit {
    latitude = 51.661535;
    longitude = 39.200287;
    locationChosen = false;
    doubleSliderPriceTrainTickets = [40000, 150000];
    doubleSliderPriceAirTickets = [40000, 150000];
    focus;
    closeResult: string;
    date: { year: number, month: number };
    public isCollapsed = true;
    public isCollapsed1 = true;
    public isCollapsed2 = true;
    public isCollapsed3 = true;
    public isCollapsed4 = true;
    public isCollapsed5 = true;
    public isCollapsed6 = true;
    public isCollapsed20 = true;
    public isCollapsedCitesTrip = true;
    public blockHotelBeforMonument = true;
    public blockTicket = true;
    country: Country[] = [];
    city: City[] = [];
    cityOnMap: City = new City();
    cityArr: City[] = [];
    monument: Monument[]= [];
    monumentToListMarker: Monument[] = [];
    countryTitle = 'Выберите страну';
    country_depart = 'Выберите страну';
    // cityTitle = 'Выберите город';
    order = [];
    map: MapComponent;
    typeMonument: Status[] = [];
    // monumentToList: PhotoMonument[] = [];
    allStatuses: Status[] = [];
    monumentToListEx = [];
    photoMonument: PhotoMonument[];
    photoMonumentCheck: PhotoMonument[] = [];
    // tslint:disable-next-line:max-line-length
    symbols: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'W', 'X', 'Y', 'Z'];
    wayPoint = [];
    enterMonument: PhotoMonument;
    dir = undefined;
    optimizeArray = [];
    optimizeWayPoints = true;
    result: any;
    show = true;
    showAgm = false;
    cluster_styles = [
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 12
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 14
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/imgonline-com-ua-Resize-JtLYjYh9fejR.png',
            textColor: 'black',
            textSize: 16
        }
    ];
    cluster_styles_hotel = [
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 12
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 14
        },
        {
            width: 66,
            height: 66,
            url: './assets/Icons/orangeClaster.png',
            textColor: 'black',
            textSize: 16
        }
    ];
    travelMode = 'WALKING';
    routeDescription: Trip[] = [];
    // переменные для отелей
    // checkHotelStar: boolean[] = [false, false, false, false, false, false];
    starHotelName: string[] = ['No star rating', '1 star', '2 star', '3 star', '4 star', '5 star'];
    masIndex: number[] = [0, 1, 2, 3, 4,  5];
    value: number;
    hotels: Hotel[] = [];
    hotelsOptimal: PhotoHotel[];
    enterHotel: PhotoHotel;
    iconUrl1 = {
        url: './assets/Icons/hotel.png',
        scaledSize: {
            width: 40,
            height: 50
        }
    };
    // optimalHotel: PhotoHotel = null;
    urlTicket: Url = new Url();
    iataFrom: string;
    iataTo: string;
    timeFrom: string;
    timeTo: string;
    allTrip: PointTrip[] = [];
    tempTripCity: PointTrip = new PointTrip();
    depCity: City[] = [];
    depContry: Country[] = [];
    delCity: string;
    iteration = 0;
    params: TicketParams;
    dateAriv: Date;
    monumentWish: PhotoMonument[];
    nameTrip: '';
    constructor(private httpService: RouteCreationService, private modalService: NgbModal) {
    }

    getListWish(context) {
        this.httpService.getWishByCity(this.tempTripCity.cityObject.nameCity).subscribe((data: PhotoMonument[]) => this.monumentWish = data, error => console.log('This is called when error occurs'), () => this.openMassegeBox(context));
    }
    onCheckStatuses() {
        for (const temp of this.allStatuses) {
            temp.checked = false;
        }
        for (const st of this.tempTripCity.monumentToList) {
            for (const temp of this.allStatuses) {
                if (temp.nameStatus === st.nameStatus) {
                    temp.checked = true;
                }
            }
        }
    }
    nullClick() {}
    onDelete(modal) {
        const tripTemp: PointTrip[] = [];
        for (const st of this.allTrip) {
            // tslint:disable-next-line:curly
            if (st.cityObject.nameCity !== this.delCity)
                tripTemp.push(st);
        }
        this.allTrip = tripTemp;
        modal.dismiss('Cross click');
    }
    // чистим все перед созданием нового города
    chooseYesOnCreateNewCity(modal) {
        this.isCollapsed = true;
        this.isCollapsed1 = false;
        this.isCollapsed2 = true;
        this.isCollapsed3 = true;
        this.isCollapsed4 = true;
        this.isCollapsed5 = true;
        this.isCollapsed6 = true;
        this.isCollapsed20 = true;
        this.isCollapsedCitesTrip = true;
        this.blockHotelBeforMonument = true;
        this.blockTicket = true;
        this.monument = [];
        // this.allTrip.push(this.tempTripCity);
        this.tempTripCity = null;
        this.tempTripCity = new PointTrip();
        this.wayPoint = [];
        for (const st of this.allStatuses) {
            st.checked  = false;
        }
        this.monumentToListEx = [];
        this.photoMonument = [];
        this.hotels = [];
        this.hotelsOptimal = [];
        modal.dismiss('Cross click')
    }

    onCrossPageProfile() {
        location.replace('/#/profile-user');
    }

    // выбран какой то город
    onChangeCity(context) {
        this.onPaintLocationCity();
        // tslint:disable-next-line:max-line-length
        this.httpService.sendCity(this.tempTripCity.cityObject.nameCity).subscribe((newList: Monument[]) => this.monument = newList);
        console.log(this.tempTripCity);
        let flag = true;
        for (const temp of this.allTrip) {
            // tslint:disable-next-line:no-unused-expression
            if (temp.cityObject === this.tempTripCity.cityObject) {
                flag = false;
            }
        }

        if (flag) {
            // this.tempTripCity.statuses = this.allStatuses;
            this.httpService.getStatus().subscribe((data: Status[]) => this.allStatuses = data);
            this.allTrip.push(this.tempTripCity);
        }
        this.getListWish(context);
        console.log(this.allTrip);
    }
    photoHotelsCheck() {
        // tslint:disable-next-line:curly
        for (const st of this.hotelsOptimal)
            // tslint:disable-next-line:curly
            if (st.placeId === this.tempTripCity.optimalHotel.placeId)
                st.check = true;
        console.log(this.hotelsOptimal);
        this.showAgm = true;
        this.wayPoint = [];
        for (let i = 0; i < this.tempTripCity.monumentToList.length; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
        }
        console.log(this.wayPoint);
        this.getDirectionWithHotel();
        this.getTicketParams();
    }
    statusesCheck() {
        for (const st of this.photoMonument) {
            for (const temp of this.tempTripCity.monumentToList) {
                if (temp.placeId === st.placeId) {
                    st.checked = true;
                }
            }
        }
        console.log('тута');
        console.log(this.photoMonument);
        console.log(this.tempTripCity);
        for (const st of this.photoMonument) {
            st.checked = false;
            for (const temp of this.tempTripCity.monumentToList) {
                // tslint:disable-next-line:curly
                if (temp.placeId === st.placeId)
                    st.checked = true;
            }
        }
        // получаем отельки
        if (this.tempTripCity.optimalHotel) {
            // tslint:disable-next-line:max-line-length
            const lon = (this.tempTripCity.monumentToList[0].longitude + this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].longitude) / 2.0;
            const lot = (this.tempTripCity.monumentToList[0].latitude + this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].latitude) / 2.0;
            this.httpService.getHotelFilterWithPoin(this.tempTripCity.cityObject.nameCity, this.tempTripCity.optimalHotel.avgPrice - 100,
                // tslint:disable-next-line:max-line-length
                this.tempTripCity.optimalHotel.avgPrice + 100, this.tempTripCity.optimalHotel.rating-0.5, this.tempTripCity.optimalHotel.rating+0.5, this.tempTripCity.checkHotelStar, lon, lot).subscribe((data: Hotel[]) => this.hotels = data,
                error => console.log('This is called when error occurs'), () => this.httpService.getPhotosHotels(this.hotels).subscribe((data: PhotoHotel[]) => this.hotelsOptimal = data,
                    () => console.log('This is called when error occurs'), () => this.photoHotelsCheck()));
        }
        // tslint:disable-next-line:one-line
        else {
            this.showAgm = true;
            console.log(this.tempTripCity.monumentToList);
            this.wayPoint = [];
            for (let i = 1; i < this.tempTripCity.monumentToList.length - 1; i++) {
                // tslint:disable-next-line:max-line-length
                this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
            }
            if (this.tempTripCity.monumentToList.length > 1) {
                this.getDirection();
            } else {
                this.showAgm = false;
            }
            this.getTicketParams();
        }
    }
    checkPhotoMonument() {
        this.photoMonument = [];
        // tslint:disable-next-line:max-line-length
        this.httpService.getPhotos(this.monument).subscribe((data: PhotoMonument[]) => this.photoMonument = data, error => console.log('This is called when error occurs'), () => this.statusesCheck());
    }
    // выбрана достопремечательность какая нибудь
    onTempTripCity(iter: number, context) {
        this.isCollapsedCitesTrip = false;
        this.blockHotelBeforMonument = false;
        this.blockTicket = false;
        this.tempTripCity = null;
        this.tempTripCity = new PointTrip();
        this.tempTripCity.cityObject = this.allTrip[iter].cityObject;
        // console.log(this.tempTripCity.cityObject);
        this.tempTripCity = this.allTrip[iter];
        this.onCheckStatuses();
        this.onPaintLocationCity();
        this.city = [];
        this.httpService.sendCountry(this.countryTitle).subscribe((newList: City[]) => {this.city = newList, this.cityArr = newList});
        this.show = true;
        this.showAgm = false;
        this.monumentToListMarker = [];
        for (let i = 0; i < this.allStatuses.length; i++) {
            // tslint:disable-next-line:curly
            if (this.allStatuses[i].checked === true) {
                this.typeMonument.push(this.allStatuses[i]);
            }
        }
        if (this.typeMonument.length !== 0) {
            // tslint:disable-next-line:max-line-length
            this.httpService.filterMonuments(this.tempTripCity.cityObject.nameCity, this.typeMonument)
                .subscribe((data: Monument[]) => this.monument = data, error => console.log('This is called when error occurs'), () => this.checkPhotoMonument());
        } else {
            this.onChangeCity(context);
        }
        this.typeMonument = [];
    }
    putParams() {
        (<HTMLSelectElement>document.getElementById('departure-country')).value = this.country_depart;
        (<HTMLSelectElement>document.getElementById('departure-city')).value = this.params.cityDeparture;
        (<HTMLSelectElement>document.getElementById('arrival-city')).value = this.params.cityArrival ;
        (<HTMLInputElement>document.getElementById('departure-date')).value = this.params.dateDeparture.getDate().toString();
        console.log(this.params.dateDeparture.getDate().toString());
        (<HTMLInputElement>document.getElementById('arrival-date')).value = this.params.dateArrival.getDate().toString();
    }
    getTicketParams() {
        if (this.urlTicket)
            this.httpService.getUrlTicket(this.urlTicket.url).subscribe((data: TicketParams) => this.params = data, error => console.log('This is called when error occurs'), () => this.putParams());
        console.log(this.params);
    }
    // какая то функция для формирования url билетов
    aviaUrl(context) {
        // tslint:disable-next-line:max-line-length
        // this.httpService.getIata((<HTMLSelectElement>document.getElementById('departure-city')).value).subscribe((data: Iata) => this.iataFrom = data);
        // this.httpService.getIata((<HTMLSelectElement>document.getElementById('arrival-city')).value).subscribe((data: Iata) => this.iataTo = data);
        this.iataFrom = (<HTMLSelectElement>document.getElementById('departure-city')).value;
        this.iataTo = (<HTMLSelectElement>document.getElementById('arrival-city')).value;
        this.timeFrom = (<HTMLInputElement>document.getElementById('departure-date')).value;
        this.timeTo = (<HTMLInputElement>document.getElementById('arrival-date')).value;
        this.httpService.getUrlAvia(
            this.iataFrom,
            this.iataTo,
            this.timeFrom,
            this.timeTo
        ).subscribe((data: Url) => this.urlTicket = data, error => console.log('This is called when error occurs'), () => this.tempTripCity.urlTicket = this.urlTicket.url);
        this.openMassegeBox(context);
    }

    // получаем города для авиабилетов
    onDepCityChange() {
        console.log(this.country_depart);
        // tslint:disable-next-line:max-line-length
        this.httpService.sendCountry(this.country_depart).subscribe((citList: City[]) => this.depCity = citList);
    }
    // возвращаем номер города в списке выбранных
    returnIterCity(str: string) {
        let j = 0;
        for (const temp of this.allTrip) {
            // tslint:disable-next-line:curly
            if (str === temp.cityObject.nameCity)
                return j;
            // tslint:disable-next-line:curly
            else j++;
        }
    }
    // отрисовка достопремичательностей без отеля
    public getDirection() {
        this.dir = undefined;
        this.dir = {
            origin: {lat: this.tempTripCity.monumentToList[0].latitude, lng: this.tempTripCity.monumentToList[0].longitude},
            // tslint:disable-next-line:max-line-length
            destination: {
                lat: this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].latitude,
                lng: this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].longitude
            },
            points: this.wayPoint
        }
    }
    // отрисовка достопремичательностей с отелем
    public getDirectionWithHotel() {
        this.dir = undefined;
        this.dir = {
            origin: {lat: this.tempTripCity.optimalHotel.latitude, lng: this.tempTripCity.optimalHotel.longitude},
            // tslint:disable-next-line:max-line-length
            destination: {
                lat: this.tempTripCity.optimalHotel.latitude,
                lng: this.tempTripCity.optimalHotel.longitude
            },
            points: this.wayPoint
        }
    }
    // регулировка открытия блоков
    collapseHotel() {
        if (this.blockHotelBeforMonument) {
            return true;
        }
        if (this.isCollapsed2) {
            return true;
        }
        return false;
    }
    collapseTicketAir() {
        if (this.blockTicket) {
            return true;
        }
        if (this.isCollapsed) {
            return true;
        }
        return false;
    }
    // обычный инит
    ngOnInit() {
        this.httpService.getCountries().subscribe((data: Country[]) => {this.country = data, this.depContry = data});
    }
    // регулировка выбора информации в блоках
    onChooseYesOnContainerHotel(modal) {
        this.isCollapsed = true;
        this.isCollapsed1 = true;
        this.isCollapsed4 = true;
        this.isCollapsed5 = true;
        this.isCollapsed20 = true;
        this.blockHotelBeforMonument = false;
        this.isCollapsed2 = false;
        this.monument = [];
        modal.dismiss('Cross click')
    }
    onChooseYesOnContainerTicket(modal) {
        this.isCollapsed1 = true;
        this.isCollapsed2 = true;
        this.isCollapsed4 = true;
        this.isCollapsed5 = true;
        this.isCollapsed20 = true;
        this.blockTicket = false;
        this.isCollapsed = false;
        modal.dismiss('Cross click')
    }
    onChooseNoOnContainerTicket(content, modal) {
        modal.dismiss('Cross click');
        this.openMassegeBox(content)
    }
    onChooseNoOnContainer(content, modal) {
        modal.dismiss('Cross click');
        this.openMassegeBox(content)
    }

    // оптимизирование достопремечателностей
    onOptimize() {
        this.optimizeWayPoints = true;
        this.show = false;
        this.showAgm = true;
        this.wayPoint = [];
        for (let i = 1; i < this.tempTripCity.monumentToList.length - 1; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
        }
        this.getDirection();
    }

    // фильтрация достопремечательностей
    onFilter(component) {
        this.show = true;
        this.showAgm = false;
        this.tempTripCity.monumentToList = [];
        this.monumentToListMarker = [];
        for (let i = 0; i < this.allStatuses.length; i++) {
            // tslint:disable-next-line:curly
            if (this.allStatuses[i].checked === true) {
                this.typeMonument.push(this.allStatuses[i]);
            }
        }
        if (this.typeMonument.length !== 0) {
            // tslint:disable-next-line:max-line-length
            this.httpService.filterMonuments(this.tempTripCity.cityObject.nameCity, this.typeMonument)
                .subscribe((data: Monument[]) => this.monument = data, error => console.log('This is called when error occurs'), () => this.onPhotoMonuments());
        } else {
            this.onChangeCity(component);
        }
        this.typeMonument = [];
    }
    // добавление в лист желаемых
    onListAdd(placeId) {
        // tslint:disable-next-line:curly

        this.show = true;
        this.showAgm = true;
        this.tempTripCity.monumentToList = [];
        for (let i = 0; i < this.photoMonument.length; i++) {
            // tslint:disable-next-line:curly
            if (this.photoMonument[i].checked === true) {
                this.tempTripCity.monumentToList.push(this.photoMonument[i]);
            }
        }
        this.monumentToListMarker = [];
        for (let i = 0; i < this.tempTripCity.monumentToList.length; i++) {
            for (let j = 0; j < this.monument.length; j++)
                // tslint:disable-next-line:curly one-line
            {
                if (this.tempTripCity.monumentToList[i].placeId === this.monument[j].placeId) {
                    this.monumentToListMarker.push(this.monument[j]);
                }
            }
        }
        this.wayPoint = [];
        for (let i = 1; i < this.tempTripCity.monumentToList.length - 1; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
        }
        if (this.tempTripCity.monumentToList.length > 1) {
            this.getDirection();
        } else {
            this.showAgm = false;
        }
    }
    // вывод меседж боксов
    openMassegeBoxHotel(content) {
        // tslint:disable-next-line:curly
        if (this.blockHotelBeforMonument === true) {
            this.openMassegeBox(content);
        }
    }
    openMassegeBox(content) {
        console.log(this.monumentWish);
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.monument = [];
    }

    openMassegeForDelCity(content, nameCity: string) {
        this.delCity = nameCity;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.monument = [];
    }
    // добавление отеля в путь + вывод
    onConfirm(placeId: string) {
        for (const st of this.hotelsOptimal) {
            if (st.placeId === placeId) {
                this.tempTripCity.optimalHotel = st;
            }
            // tslint:disable-next-line:one-line
            else {
                st.check = false;
            }
        }
        console.log(this.tempTripCity.optimalHotel);
        console.log(this.hotelsOptimal);

        this.showAgm = true;
        this.wayPoint = [];
        for (let i = 0; i < this.tempTripCity.monumentToList.length; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
        }
        console.log(this.wayPoint);
        this.getDirectionWithHotel();
    }
    // получаем фотки на достопремечательности
    onPhotoMonuments() {
        // tslint:disable-next-line:max-line-length
        this.httpService.getPhotos(this.monument).subscribe((data: PhotoMonument[]) => this.photoMonument = data, error => console.log('This is called when error occurs'), () => console.log(this.photoMonument));
    }
    // выбрана страна получаем города
    onChange() {
        this.httpService.sendCountry(this.countryTitle).subscribe((newList: City[]) => {this.city = newList, this.cityArr = newList});
        this.tempTripCity.tempCountry = this.countryTitle;
        console.log(this.cityArr);
    }
    // вывод города на карте
    onPaintLocationCity() {
        this.latitude = this.tempTripCity.cityObject.latitude;
        this.longitude = this.tempTripCity.cityObject.longitude;
        this.locationChosen = true;
    }

    // открытие блока достопремечательностей
    open(content, movie: PhotoMonument) {
        this.isCollapsed6 = true;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.enterMonument = movie;
    }
    // открытие блока отелей
    openHotel(content, movie: PhotoHotel) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        console.log(this.enterHotel);
        this.enterHotel = movie;
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    // удаление достопремеч из списка желаемых
    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.tempTripCity.monumentToList, event.previousIndex, event.currentIndex);
        this.wayPoint = [];
        for (let i = 1; i < this.tempTripCity.monumentToList.length - 1; i++) {
            // tslint:disable-next-line:max-line-length
            this.wayPoint.push({location: {lat: this.tempTripCity.monumentToList[i].latitude, lng: this.tempTripCity.monumentToList[i].longitude}, stopover: true});
        }
        this.optimizeWayPoints = false;
        this.show = false;
        this.showAgm = true;
        this.getDirection();
    }

    onChoseLocation(event) {
        this.latitude = event.coords.lat;
        this.longitude = event.coords.lng;
        this.locationChosen = true;
    }

    onMouseOver(infoWindow, gm) {
        if (gm.lastOpen != null) {
            gm.lastOpen.close();
        }
        gm.lastOpen = infoWindow;
        infoWindow.open();
    }

    // страшная Олина функция, лень разбирать
    onResponse(event) {
        console.log(event);
        this.optimizeArray = [];
        this.routeDescription = [];
        this.order = event.routes[0].waypoint_order;
        for (let i = 0; i < this.tempTripCity.monumentToList.length - 2; i++) {
            this.optimizeArray.push(this.wayPoint[this.order[i]]);
            this.monumentToListEx[i] = this.tempTripCity.monumentToList[this.order[i] + 1];
        }
        for (let i = 1; i < this.tempTripCity.monumentToList.length - 1; i++) {
            this.tempTripCity.monumentToList[i] = this.monumentToListEx[i - 1];
        }
        if (this.tempTripCity.optimalHotel) {
            for (let i = 0; i < event.routes[0].legs.length; i++) {
                this.routeDescription[i] = new Trip();
                if (i === 0 && this.tempTripCity.optimalHotel) {
                    this.routeDescription[i].start = this.tempTripCity.optimalHotel.nameHotel;
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.monumentToList[i].nameMonument.substr(1, this.tempTripCity.monumentToList[i].nameMonument.length - 2);
                } else if (i === event.routes[0].legs.length - 1 && this.tempTripCity.optimalHotel !== null) {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].start = this.tempTripCity.monumentToList[i - 1].nameMonument.substr(1, this.tempTripCity.monumentToList[i - 1].nameMonument.length - 2);
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.optimalHotel.nameHotel;
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].start = this.tempTripCity.monumentToList[i - 1].nameMonument.substr(1, this.tempTripCity.monumentToList[i - 1].nameMonument.length - 2);
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].end = this.tempTripCity.monumentToList[i].nameMonument.substr(1, this.tempTripCity.monumentToList[i].nameMonument.length - 2);
                }
                this.routeDescription[i].distance = event.routes[0].legs[i].distance.value / 1000 + ' km ';
                const value = event.routes[0].legs[i].duration.value;
                if (Math.trunc(value / 3600) === 0) {
                    this.routeDescription[i].duration = Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].duration = Math.trunc(value / 3600) + ' h ' + Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                }
                for (let j = 0; j < event.routes[0].legs[i].steps.length; j++) {
                    const step = new Steps();
                    step.distance = event.routes[0].legs[i].steps[j].distance.value / 1000 + ' km ';
                    const valueSteps = event.routes[0].legs[i].steps[j].duration.value;
                    if (Math.trunc(valueSteps / 3600) === 0) {
                        step.duration = Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    } else {
                        // tslint:disable-next-line:max-line-length
                        step.duration = Math.trunc(valueSteps / 3600) + ' h ' + Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    }
                    step.maneuver = event.routes[0].legs[i].steps[j].maneuver;
                    this.routeDescription[i].steps.push(step);
                }
            }
            console.log(this.routeDescription);
        } else {
            for (let i = 0; i < event.routes[0].legs.length; i++) {
                this.routeDescription[i] = new Trip();
                // tslint:disable-next-line:max-line-length
                this.routeDescription[i].start = this.tempTripCity.monumentToList[i].nameMonument.substr(1, this.tempTripCity.monumentToList[i].nameMonument.length - 2);
                // tslint:disable-next-line:max-line-length
                this.routeDescription[i].end = this.tempTripCity.monumentToList[i + 1].nameMonument.substr(1, this.tempTripCity.monumentToList[i + 1].nameMonument.length - 2);
                this.routeDescription[i].distance = event.routes[0].legs[i].distance.value / 1000 + ' km ';
                const value = event.routes[0].legs[i].duration.value;
                if (Math.trunc(value / 3600) === 0) {
                    this.routeDescription[i].duration = Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                } else {
                    // tslint:disable-next-line:max-line-length
                    this.routeDescription[i].duration = Math.trunc(value / 3600) + ' h ' + Math.round((value - Math.trunc(value / 3600) * 3600) / 60) + ' min';
                }
                for (let j = 0; j < event.routes[0].legs[i].steps.length; j++) {
                    const step = new Steps();
                    step.distance = event.routes[0].legs[i].steps[j].distance.value / 1000 + ' km ';
                    const valueSteps = event.routes[0].legs[i].steps[j].duration.value;
                    if (Math.trunc(valueSteps / 3600) === 0) {
                        step.duration = Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    } else {
                        // tslint:disable-next-line:max-line-length
                        step.duration = Math.trunc(valueSteps / 3600) + ' h ' + Math.round((valueSteps - Math.trunc(valueSteps / 3600)) / 60) + ' min';
                    }
                    step.maneuver = event.routes[0].legs[i].steps[j].maneuver;
                    this.routeDescription[i].steps.push(step);
                }
            }
            console.log(this.routeDescription);
        }
    }
    closeModalEnd(modal) {
        modal.dismiss('Cross click');
    }
    // сохранение в профиль, пока тут заглушка
    saveInProfile(nameTrip: string, modal, context) {
        let allTrip: TripOneCheckPoint[] = [];
        for (const st of this.allTrip) {
            let resultStar = 1;
            console.log(st);
            let oneTrip: TripOneCheckPoint = new TripOneCheckPoint();
            if(st.urlTicket)
                oneTrip.url = st.urlTicket;
            if(st.optimalHotel) {
                oneTrip.hotel = st.optimalHotel;
                for (let temp = st.checkHotelStar.length - 1; temp >= 0 ; temp--) {
                    // tslint:disable-next-line:curly triple-equals
                    if (st.checkHotelStar[temp] == true)
                        resultStar = resultStar * 10 + 1;
                    // tslint:disable-next-line:curly
                    else resultStar = resultStar * 10;
                }
                oneTrip.stars = resultStar;
            }
            oneTrip.listMonument = st.monumentToList;
            allTrip.push(oneTrip);
        }
        this.httpService.sendResultTrip(allTrip,  localStorage.getItem('id'), nameTrip).subscribe();
        this.onCrossPageProfile();
        modal.dismiss('Cross click');
        this.openMassegeBox(context);
    }

    // скачать все, что есть, надо править
    download() {
        const doc = new jsPDF();
        let j = 20;
        doc.setFontSize(20);
        doc.setFontStyle('Impact');
        doc.setFontType('italic');
        doc.text('Description of your journey' + '\n', 70, j)
        j += 10;
        doc.setFontSize(14);
        // tslint:disable-next-line:triple-equals
        if (this.tempTripCity.optimalHotel != null) {
            doc.text('Your hotel: ' + this.tempTripCity.optimalHotel.nameHotel + '\n', 20, j);
            j += 10;
        }
        for (let i = 0; i < this.routeDescription.length; i++) {
            doc.text('Your start place: ' + this.routeDescription[i].start + '\n', 20, j);
            j += 10;
            doc.text('Your end place: ' + this.routeDescription[i].end + '\n', 20, j);
            j += 10;
            doc.text('Your general distance: ' + this.routeDescription[i].distance + '\n', 20, j);
            j += 10;
            doc.text('Your general time: ' + this.routeDescription[i].duration + '\n', 20, j);
            j += 10;
            doc.setTextColor(0, 0, 255);
            doc.text('Your steps: ' + '\n', 20, j);
            j += 10;
            doc.setTextColor(0)
            for (let m = 0; m < this.routeDescription[i].steps.length; m++) {
                if (m === 22) {
                    doc.addPage();
                    j = 20;
                }
                if (this.routeDescription[i].steps[m].maneuver === '') {
                    // tslint:disable-next-line:max-line-length
                    doc.text('     ' + (m + 1) + ') go straight ' + this.routeDescription[i].steps[m].distance + '(around ' + this.routeDescription[i].steps[m].duration + ')' + '\n', 20, j);
                    j += 10;
                } else {
                    // tslint:disable-next-line:max-line-length
                    doc.text('     ' + (m + 1) + ') ' + this.routeDescription[i].steps[m].maneuver + ' ' + this.routeDescription[i].steps[m].distance + '(around ' + this.routeDescription[i].steps[m].duration + ')' + '\n', 20, j);
                    j += 10;
                }
            }
            doc.addPage();
            j = 20;
        }
        doc.save('Journey description.pdf');
    }
    // фильтрация отелей
    onFilterForHotel() {
        // tslint:disable-next-line:prefer-const
        console.log(this.tempTripCity.checkHotelStar);
        // tslint:disable-next-line:max-line-length
        const lon = (this.tempTripCity.monumentToList[0].longitude + this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].longitude) / 2.0;
        const lot = (this.tempTripCity.monumentToList[0].latitude + this.tempTripCity.monumentToList[this.tempTripCity.monumentToList.length - 1].latitude) / 2.0;
        console.log(lon, lot);

        this.httpService.getHotelFilterWithPoin(this.tempTripCity.cityObject.nameCity, this.tempTripCity.doubleSliderPriceHotel[0],
            // tslint:disable-next-line:max-line-length
            this.tempTripCity.doubleSliderPriceHotel[1], this.tempTripCity.doubleSliderRating[0], this.tempTripCity.doubleSliderRating[1], this.tempTripCity.checkHotelStar, lon, lot).subscribe((data: Hotel[]) => this.hotels = data,
            error => console.log('This is called when error occurs'), () => this.onPhotoHotels());
        console.log(this.hotels);
    }
    // получение отелей с фотками
    onPhotoHotels() {
        this.httpService.getPhotosHotels(this.hotels).subscribe((data: PhotoHotel[]) => this.hotelsOptimal = data);
    }
    // открытие новых страничек
    routing(placeId: string) {
        console.log(placeId);
        HotelPageComponent.placeId = placeId;
    }
}
