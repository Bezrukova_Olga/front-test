import {Component, Input, OnInit} from '@angular/core';
import {MonumentPageService} from './monumentPage.service';
import {AllPhotoMonument} from './dto/monumentPage.photo';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {MonumentPageComment} from '../dto/monumentPage.comment';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {MonumentPageResponse} from '../dto/MonumentPageResponse';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {RouteCreationService} from '../routeCreation/routeCreation.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-monument',
    templateUrl: './monumentPage.component.html',
    styleUrls: ['./monumentPage.component.scss'],
    providers: [MonumentPageService]
})
export class MonumentPageComponent implements OnInit {
    static placeId = '';
    photoMonument: AllPhotoMonument;
    url: string[] = [];
    change = false;
    show = false;
    comment = '';
    photoUser: string[];
    status: string;
    id: string;
    userComment = false;
    time: string[] = [];
    date: string[] = [];
    private subscription: Subscription;
    selectItem: MonumentPageResponse;
    selectPhoto: string;
    @Input()
    responses: Array<any>;
    monumentComment: MonumentPageComment;
    private hasBaseDropZoneOver = false;
    uploader: FileUploader;
    closeResult: string;
    uploaderOptions: FileUploaderOptions = {
        url: 'https://api.cloudinary.com/v1_1/daqgh2ufv/image/upload',
        autoUpload: true,
        isHTML5: true,
        removeAfterUpload: false,
        headers: [
            {
                name: 'X-Requested-With',
                value: 'XMLHttpRequest'
            }
        ]
    };
    checkWish = false;
    allComments: MonumentPageResponse[] = [];
    photoYes = false;
    idMonument: string;

    constructor(private httpService: MonumentPageService, private activateRoute: ActivatedRoute, private modalService: NgbModal) {
        this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
    }

    ngOnInit(): void {
        if (this.id !== '') {
            // tslint:disable-next-line:max-line-length
            this.httpService.getAllPhotos(this.id).subscribe((data: AllPhotoMonument) => this.photoMonument = data, error => console.log('This is called when error occurs'), () => this.cost());
        }
        this.httpService.getComment(this.id).subscribe((data: MonumentPageResponse[]) => this.allComments = data,
                error => console.log('This is called when error occurs'), () => this.userCheck());
        this.uploader = new FileUploader(this.uploaderOptions);

        this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
            form.append('upload_preset', 'ml_default');
            form.append('file', fileItem);
            fileItem.withCredentials = false;
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            const responsePath = JSON.parse(response);
            this.url.push(responsePath.url);
            console.log(responsePath.url);
        };

    }

    checkCheckWish() {
        this.httpService.addWish(this.id).subscribe();
        this.checkWish = !this.checkWish;
        return this.checkWish;
    }
    shearchWish() {
        this.httpService.getWishSearch(this.id).subscribe((data: boolean) => this.checkWish = data);
    }
    deleteWish() {
        this.httpService.getWishDel(this.id).subscribe((data: boolean) => this.checkWish = data);
    }
    cost() {
        this.show = true;
        this.shearchWish();
    }

    userCheck() {
        const idTempUser = localStorage.getItem('id');
        for (let i = 0; i < this.allComments.length; i++ ) {
            if (this.allComments[i].idUser === idTempUser) {
                this.allComments[i].check = true;
            } else {
                this.allComments[i].check = false;
            }
            if (this.allComments[i].photoUser === '') {
                this.allComments[i].photoUser = 'https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375';
            }
        }
        this.updateTime();
    }
    open(content, movie: MonumentPageResponse) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.selectItem = movie;
    }

    openPhoto(content, movie: string) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
        this.selectPhoto = movie;
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addComment() {
        const date = new Date();
        console.log(date);
        this.comment = (<HTMLInputElement>document.getElementById('textArea')).value;
        this.httpService.addComment(localStorage.getItem('id').toString(), this.id, this.comment, this.url, date)
            .subscribe((data: MonumentPageComment) => this.monumentComment = data,
                error => console.log('This is called when error occurs'),
                () => this.updateComments());
        (<HTMLInputElement>document.getElementById('textArea')).value = '';
        this.url = [];
    }

    updateComments() {
        this.httpService.getComment(this.id).subscribe((data: MonumentPageResponse[]) => this.allComments = data,
            error => console.log('This is called when error occurs'),
            () => this.userCheck());
    }

    updateTime() {
        for (let i = 0; i < this.allComments.length; i++) {
            if (this.allComments[i].date !== null) {
                this.time[i] = new Date(this.allComments[i].date).toLocaleTimeString();
                this.date[i] = new Date(this.allComments[i].date).toLocaleDateString();
            } else {
                this.time[i] = '';
                this.date[i] = '';
            }
        }
    }

    deletePhoto(item: string) {
        this.url.splice(this.url.indexOf(item), 1);
    }

    deleteComment(content) {
        if (this.selectItem.idUser === localStorage.getItem('id')) {
            this.httpService.deleteComment(this.selectItem.idComment).subscribe((data: string) => this.status = data);
        }
        this.allComments.splice(this.allComments.indexOf(this.selectItem), 1);
        this.modalService.dismissAll();
    }
}
