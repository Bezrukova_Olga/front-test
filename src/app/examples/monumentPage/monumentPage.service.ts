import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AllPhotoMonument} from './dto/monumentPage.photo';
import {PhotoMonument} from '../dto/routeCreation.photoMonument';
import {AllPhotoHotel} from '../hotelPage/dto/hotelPage.photo';

@Injectable()
export class MonumentPageService {
    constructor(private http: HttpClient) {}

    getAllPhotos(placeId: string) {
        return this.http.get<AllPhotoMonument>('http://localhost:8080/photos/' + placeId);
    }

    getAllPhotosByMonument(placeId: string) {
        return this.http.get<AllPhotoMonument>('http://localhost:8080/photos/id/' + placeId);
    }

    getMonumentInfo(placeId: string) {
        return this.http.get<PhotoMonument[]>('http://localhost:8080/search/monuments/info/' + placeId);
    }

    getWishSearch(placeId: string) {
        return this.http.get<boolean>('http://localhost:8080/wish/search?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    getWishDel(placeId: string) {
        return this.http.get<boolean>('http://localhost:8080/wish/delete?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    addWish(placeId: string) {
        return this.http.get<void>('http://localhost:8080/wish/add?userid=' + localStorage.getItem('id') + '&idPlaceMonument=' + placeId);
    }
    addComment(idUser: string, idMonument: string, comment: string, photo: string[], date: Date) {
        const body = {
            idUser: idUser,
            idMonument: idMonument,
            comment: comment,
            photo: photo,
            date: date
        };
        return this.http.post('http://localhost:8080/comments/monument', body);
    }

    getComment(id: string) {
        return this.http.get('http://localhost:8080/comments?idMonument=' + id);
    }

    deleteComment(idComment: string) {
        return this.http.delete('http://localhost:8080/comments/' + idComment);
    }

    getAllPhotosHotel(placeId: string) {
        return this.http.get<AllPhotoHotel>('http://localhost:8080/hotel/photos/' + placeId);
    }

    addCommentHotel(idUser: string, idHotel: string, comment: string, photo: string[], date: Date) {
        const body = {
            idUser: idUser,
            idHotel: idHotel,
            comment: comment,
            photo: photo,
            date: date
        };
        return this.http.post('http://localhost:8080/comments/hotels', body);
    }

    getCommentHotel(id: string) {
        return this.http.get('http://localhost:8080/comments/id?idHotel=' + id);
    }

    deleteCommentHotel(idComment: string) {
        return this.http.delete('http://localhost:8080/comments/id/' + idComment);
    }
}
