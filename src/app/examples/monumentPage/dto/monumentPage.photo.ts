import {WorkTimeMonument} from '../../dto/routeCreation.workTimeMonument';

export class AllPhotoMonument {
    nameMonument: string;
    photo: string[];
    placeId: string;
    monumentInfo: string;
    workTimeMonument: WorkTimeMonument[];
}
