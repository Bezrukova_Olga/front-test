import {Component, OnInit} from '@angular/core';
import {General} from './general';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {Subscription, timer} from 'rxjs';
import {Routes} from './user-profile.routes';
import {User} from '../dto/routeCreation.user';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {TripForLenta} from '../dto/tripForLenta';
import {ShowWayComponent} from '../showWay/showWay.component';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PointTrip} from '../dto/routeCreation.pointTrip';
import {Issub} from "./issub";
import {Sub} from "../user-followers/sub";
import {Role} from "./role";
import {BanStatus} from "./ban-status";
import {WebsocketChatMessage} from "../websocket-chat/websocket-chat.message";
import {WebSocketAPI} from "../websocket-chat/WebSocketApi";

@Component({
    selector: 'app-profile-new',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
    general: General;
    routes: Routes[] = [];
    subsRoutes: Routes[] = [];
    oneSubRoutes: Routes[] = [];
    user: User;
    role: Role;
    status: string;
    idnum: number;
    id: string;
    subs: Sub[] = [];
    isSubOn: boolean;
    storage: Storage
    isAdmin: boolean;
    isModer: boolean;
    isBanned: boolean;
    currentUserModer: boolean;
    private headers: HttpHeaders = new HttpHeaders();
    followers: string;
    change = false;
    url = '';
    private subscription: Subscription;
    httpOptions = {
        headers: new HttpHeaders({
            'Id': localStorage.getItem('id'),
            'Token': localStorage.getItem('token')
        })
    };
    uploader: FileUploader;
    closeResult: string;
    uploaderOptions: FileUploaderOptions = {
        url: 'https://api.cloudinary.com/v1_1/daqgh2ufv/image/upload',
        autoUpload: true,
        isHTML5: true,
        removeAfterUpload: false,
        headers: [
            {
                name: 'X-Requested-With',
                value: 'XMLHttpRequest'
            }
        ]
    };
    allLentaUser: TripForLenta[] = [];
    delCity: string;
    constructor(public http: HttpClient, private activateRoute: ActivatedRoute, private modalService: NgbModal) {
        this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
    }

    getUserInfo() {
        // tslint:disable-next-line:max-line-length
        this.http.get('http://localhost:8080/user/general/?id=' + this.id, this.httpOptions).subscribe((nick: General) => this.general = nick);
    }
    getInformation() {
        this.http.get<TripForLenta[]>('http://localhost:8080/trip/user/' + this.id + '/all').subscribe((data: TripForLenta[]) => this.allLentaUser = data,
                error => console.log('This is called when error occurs'), () => console.log(this.allLentaUser));
    }
    helloMessage() {
        let message: WebsocketChatMessage;
        message = new WebsocketChatMessage();
        message.date = Date.now();
        message.content = 'hello';
        message.sender = localStorage.getItem('id');
        message.recipient = this.id;
        this.http.post('http://localhost:8080/messages/post/?sender='
             + message.sender + '&recipient=' + message.recipient +
            '&content=' + message.content, null, this.httpOptions).subscribe((data: String) => {
                console.log('succesed');
                location.replace('/#/chat')
        });

    }

    ngOnInit(): void {
        this.idnum = 0;
        if(localStorage.getItem('role')=='admin'){
            this.isAdmin = true;
            this.isModer = true;
        }
        else if (localStorage.getItem('role')=='moderator'){
            this.isModer = true;
        }
        else {
            this.isAdmin = false;
            this.isModer = false;
        }
        this.storage = localStorage;
        this.general = new General();
        if(!location.href.includes('?id=')){
            this.id = localStorage.getItem('id');
            location.href += ('?id=' + this.id);
        } else {
            this.idnum = location.href.indexOf('?id=');
            this.id = location.href.substring(this.idnum + 4, location.href.length);
            if(this.id != localStorage.getItem('id')){
                this.checkSub();
            }
        }
        this.getRoutes();
        this.getSubs();
        this.getRole();
        this.checkBan();
        this.followers = '/#/followers?id=' + localStorage.getItem('id');
        this.getUserInfo();
        this.uploader = new FileUploader(this.uploaderOptions);

        this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
            form.append('upload_preset', 'ml_default');
            form.append('file', fileItem);
            fileItem.withCredentials = false;
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            const responsePath = JSON.parse(response);
            this.url = responsePath.url;
            this.updatePhoto();
        };
        this.getInformation();
    }

    showTrip(id: string) {
        ShowWayComponent.placeId = id;
    }
    getRole() {
        this.http.get('http://localhost:8080/user/role/?id=' + this.id, this.httpOptions).subscribe(
            (data: Role) => {
                this.role = data;
                if(this.role.role=='moderator'){
                    this.currentUserModer = true;
                } else {
                    this.currentUserModer = false;
                }
            }
        )
    }

    changeRole(newRole: string) {
        this.http.put('http://localhost:8080/user/role/?id=' + this.id +
            '&role=' + newRole, null, this.httpOptions
        ).subscribe(()=> {this.getRole()})
    }

    ban() {
        this.http.put('http://localhost:8080/user/ban/?id=' + this.id, null, this.httpOptions
        ).subscribe(
            () => this.checkBan()
        );
    }

    unban() {
        this.http.put('http://localhost:8080/user/unban/?id=' + this.id, null, this.httpOptions
        ).subscribe(
            () => this.checkBan()
        );
    }

    checkBan() {
        this.http.get('http://localhost:8080/user/now/?id=' + this.id, this.httpOptions).subscribe(
            (data: BanStatus) => {
                if(data.status=='banned'){
                    this.isBanned = true;
                } else {
                    this.isBanned = false;
                }
            }
        )
    }

    updatePhoto() {
        this.http.get('http://localhost:8080/user/photo?name=' + localStorage.getItem('id') + '&change=' + this.url)
            .subscribe((data: string) => this.status = data);
    }

    deleteTrip(idTrip: string) {
        let bool: boolean;
        this.http.get('http://localhost:8080/trip/delete/' + idTrip).subscribe((data: boolean) => bool = data);
        return bool;
    }

    openMassegeForDelCity(content, nameCity: string) {
        this.delCity = nameCity;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    checkID() {
        console.log(this.id);
        if (this.id === localStorage.getItem('id'))
            return true;
        return false;
    }
    nullClick() {}
    chooseStatus(id: string, status: boolean) {
        if(this.checkID()) {
            let stat = true;
            if(status === null || status === false)
                stat = false;
            console.log(id);
            console.log(this.allLentaUser);
            for(const st of this.allLentaUser) {
                if(id  == st.id) {
                    if(st.status === null || st.status === false) {
                        st.status = true;
                    }
                    else {
                        st.status = false;
                    }
                }
            }
            console.log(this.allLentaUser);
            this.http.get<void>('http://localhost:8080/trip/status?userid=' + id + '&status=' + stat).subscribe();
            return;
        }
    }
    onDelete(modal) {
            this.deleteTrip(this.delCity);
            const tripTemp: TripForLenta[] = [];
            for (const st of this.allLentaUser) {
                // tslint:disable-next-line:curly
                if (st.id !== this.delCity)
                    tripTemp.push(st);
            }
            this.allLentaUser = tripTemp;
            modal.dismiss('Cross click');
    }
    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    getRoutes() {
        // tslint:disable-next-line:max-line-length
        let idOf = location.href.substring(location.href.indexOf('?id=') + 4, location.href.length);
        this.http.get('http://localhost:8080/user/routes/?id=' + idOf, this.httpOptions).subscribe((data: Routes[]) => this.routes = data);
        this.http.get<User>('http://localhost:8080/search/user/?id=' + idOf).subscribe((data: User) => this.user = data, error => console.log('This is called when error occurs'), () => this.checkUser());
    }

    checkSub() {
        this.http.get('http://localhost:8080/user/checksub/?id_cur=' + localStorage.getItem('id') + '&id_on=' + this.id, this.httpOptions).subscribe(
            (data: Issub) => {
                if(data.exists=="true"){
                    this.isSubOn=true;
                }
                else {
                    this.isSubOn=false;
                }
                console.log(data.exists);
            }
        )
    }

    subscribeOn() {
        this.http.post('http://localhost:8080/user/subscription/?id_from=' + localStorage.getItem('id') +
            '&id_to=' + this.id, null, this.httpOptions
        ).subscribe(
            (data: Issub) => {
                if (data.exists === 'true') {
                    this.isSubOn = true;
                } else {
                    this.isSubOn = false;
                }
            }
        )
    }

    unsubscribeFrom() {
        this.http.delete('http://localhost:8080/user/unsub/?id_cur=' + localStorage.getItem('id') +
            '&id_from=' + this.id, this.httpOptions
        ).subscribe(
            (data: Issub) => {
                if(data.exists=="true"){
                    this.isSubOn=true;
                }
                else {
                    this.isSubOn=false;
                }
            }
        )
    }

    getSubs() {
        let idOf = location.href.substring(location.href.indexOf('?id=') + 4, location.href.length);
        this.http.get('http://localhost:8080/user/followers/?id=' + idOf, this.httpOptions).subscribe((data: Sub[]) =>
            {
                this.subs = data;
                for(let i = 0; i < this.subs.length; i++){
                    this.getSubsRoutes(this.subs[i].id);
                }
            }
        );
    }

    getSubsRoutes(id: string) {
        this.http.get('http://localhost:8080/user/routes/?id=' + id, this.httpOptions).subscribe((data: Routes[]) => {
            this.oneSubRoutes = data
            for(let i = 0; i < this.oneSubRoutes.length; i++){
                this.subsRoutes.push(this.oneSubRoutes[i]);
            }
        });
    }

    private checkUser() {
        if (this.user.photo !== null && this.user.photo !== undefined && this.user.photo !== '') {
            this.url = this.user.photo;
        } else {
            this.url = 'https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375'
        }
    }
}
