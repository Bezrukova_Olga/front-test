import {Hotel} from '../dto/routeForHotels/routeCreation.hotel';
import {TripMonumentResponse} from '../dto/routeCreation.tripMonumentResponse';
import {City} from '../dto/routeCreation.city';
import {CheckPointResponses} from '../dto/CheckPointResponses';

export class Routes {
    hotel: Hotel = new Hotel();
    listMonumentEntities: TripMonumentResponse[] = [];
    url: string;
    public nameCity: City = new City();

}
