import {Component, Injectable, OnInit} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiResponse} from "../dto/apiResponse";

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
@Injectable()
export class RegistrationComponent implements OnInit {
    test: Date = new Date();
    focus;
    focus1;
    data: ApiResponse;
    registerForm: FormGroup;
    submitted = false;
    constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
            surname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
            nickname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
            login: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
            passwordReplay: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]]
        });
    }

    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        if (this.f.password.value === this.f.passwordReplay.value) {
                const headers = new HttpHeaders()
                    .set('login', this.f.login.value)
                    .set('password', this.f.password.value);
                const body = {'nickname': this.f.nickname.value, 'name': this.f.name.value, 'surname': this.f.surname.value};
                this.http.post('http://localhost:8080/registration', body, {headers: headers})
                    .subscribe((data: ApiResponse) => {
                        this.data = data;
                        if (this.data.status === 201) {
                            location.replace('http://localhost:4200/#/after_registration?email=' + this.f.login.value);
                        } else {
                            alert(this.data.message);
                        }
                    });
        } else {
            alert('Password mismatch.');
        }
    }
}
