import {Component, OnInit, Renderer2} from '@angular/core';
import { Renderer } from  '@angular/core/esm2015/src/render3'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from "@angular/common/http";
import {Role} from "../examples/user-profile/role";

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
    ngb-progressbar {
        margin-top: 5rem;
    }
    `]
})

export class ComponentsComponent implements OnInit {
    page = 4;
    page1 = 5;
    focus;
    focus1;
    focus2;
    date: {year: number, month: number};
    model: NgbDateStruct;
    constructor( private renderer : Renderer2, private http: HttpClient) {}
    isWeekend(date: NgbDateStruct) {
        const d = new Date(date.year, date.month - 1, date.day);
        return d.getDay() === 0 || d.getDay() === 6;
    }

    isDisabled(date: NgbDateStruct, current: {month: number}) {
        return date.month !== current.month;
    }

    ngOnInit() {
        if(location.href.includes('?logged=1')){
            location.href = 'http://localhost:4200/#/home';
            this.http.get('http://localhost:8080/user/role?id=' + localStorage.getItem('id')).subscribe((data: Role) => {localStorage.setItem('role', data.role); console.log(data.role)});
        }
        let input_group_focus = document.getElementsByClassName('form-control');
        let input_group = document.getElementsByClassName('input-group');
        for (let i = 0; i < input_group.length; i++) {
            input_group[i].children[0].addEventListener('focus', function (){
                input_group[i].classList.add('input-group-focus');
            });
            input_group[i].children[0].addEventListener('blur', function (){
                input_group[i].classList.remove('input-group-focus');
            });
        }
    }

}
