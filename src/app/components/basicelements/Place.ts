export class Place {
    name: string;
    longitude: number;
    latitude: number;
    description: string;
    category: string[];
    city: string;
    country: string;
    photo: string;
    id: string;
}
