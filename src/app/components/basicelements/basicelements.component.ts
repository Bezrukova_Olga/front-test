import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PhotoMonument} from '../../examples/dto/routeCreation.photoMonument';
import {WorkTimeMonument} from '../../examples/dto/routeCreation.workTimeMonument';
import {User} from '../../examples/dto/routeCreation.user';
import {MonumentPageResponse} from '../../examples/dto/MonumentPageResponse';
import {Place} from './Place';
import {HotelPageResponse} from '../../examples/dto/HotelPageResponse';
import {environment} from '../../../environments/environment.prod';

@Component({
    selector: 'app-basicelements',
    templateUrl: './basicelements.component.html',
    styleUrls: ['./basicelements.component.scss']
})
export class BasicelementsComponent implements OnInit {
    simpleSlider = 40;
    doubleSlider = [20, 60];
    state_default = true;
    focus: any;
    centerInfo: Place = {
        name: '',
        longitude: null,
        latitude: null,
        description: '',
        category: [],
        city: '',
        country: '',
        photo: '',
        id: ''
    };
    rightBottomInfo: Place = {
        name: '',
        longitude: null,
        latitude: null,
        description: '',
        category: [],
        city: '',
        country: '',
        photo: '',
        id: ''
    };
    leftUpInfo: PhotoMonument = {
        nameMonument: '',
        photo: '',
        placeId: '',
        monumentInfo: '',
        latitude: null,
        longitude: null,
        workTimeMonument: [],
        checked: false,
        nameStatus: ''
    };
    centerBottomInfo: PhotoMonument = {
        nameMonument: '',
        photo: '',
        placeId: '',
        monumentInfo: '',
        latitude: null,
        longitude: null,
        workTimeMonument: [],
        checked: false,
        nameStatus: ''
    };
    rightMiddleInfo: User = {
        id: '',
        login: '',
        nickname: '',
        role: '',
        photo: ''
    };
    rightUpInfo: MonumentPageResponse = {
        idUser: '',
        userNickname: '',
        photoUser: '',
        photo: [],
        text: '',
        idComment: '',
        date: null,
        check: false
    };
    leftBottomInfo: MonumentPageResponse = {
        idUser: '',
        userNickname: '',
        photoUser: '',
        photo: [],
        text: '',
        idComment: '',
        date: null,
        check: false
    };
    leftMiddleInfo: HotelPageResponse = {
        idUser: '',
        userNickname: '',
        photoUser: '',
        photo: [],
        text: '',
        idComment: '',
        date: null,
        check: false
    };
    flag = true;

    constructor(public http: HttpClient) {
    }

    ngOnInit() {
        this.http.get(environment.baseUrl + '/home').subscribe((data: PhotoMonument) => this.leftUpInfo = data,
            error => console.log('This is called when error occurs'), () => this.cost());
        this.http.get(environment.baseUrl + '/home/place').subscribe((data: Place) => this.centerInfo = data,
            error => console.log('This is called when error occurs'), () => this.placeCheck());
        this.http.get(environment.baseUrl + '/home/place').subscribe((data: Place) => this.rightBottomInfo = data,
            error => console.log('This is called when error occurs'), () => this.placeCheck());
        this.http.get(environment.baseUrl + '/home/user').subscribe((data: User) => this.rightMiddleInfo = data,
            error => console.log('This is called when error occurs'), () => this.userCheck());
        this.http.get(environment.baseUrl + '/home/comment').subscribe((data: MonumentPageResponse) => this.rightUpInfo = data,
            error => console.log('This is called when error occurs'), () => this.commentCheck());
        this.http.get(environment.baseUrl + '/home/comment').subscribe((data: MonumentPageResponse) => this.leftBottomInfo = data,
            error => console.log('This is called when error occurs'), () => this.commentCheck());
        this.http.get(environment.baseUrl + '/home').subscribe((data: PhotoMonument) => this.centerBottomInfo = data,
            error => console.log('This is called when error occurs'), () => this.cost());
        this.http.get(environment.baseUrl + '/home/commentHotel').subscribe((data: HotelPageResponse) => this.leftMiddleInfo = data,
            error => console.log('This is called when error occurs'), () => this.cost());
    }

    cost() {
        this.flag = false;
    }

    placeCheck() {
        if (this.rightBottomInfo.name === this.centerInfo.name) {
            this.rightBottomInfo = {
                name: '',
                longitude: null,
                latitude: null,
                description: '',
                category: [],
                city: '',
                country: '',
                photo: '',
                id: ''
            };
        }
    }

    viewProfile(id: string) {
        location.replace('/#/profile-user?id=' + id);
    }

    userCheck() {
        if (this.rightMiddleInfo.photo === '') {
            this.rightMiddleInfo.photo = 'https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375';
        }
    }

    commentCheck() {
        console.log(this.rightUpInfo);
        console.log(this.leftBottomInfo);
        if (this.rightUpInfo.text === this.leftBottomInfo.text) {
            this.leftBottomInfo = {
                idUser: '',
                userNickname: '',
                photoUser: '',
                photo: [],
                text: '',
                idComment: '',
                date: null,
                check: false
            };
        }
    }

}
