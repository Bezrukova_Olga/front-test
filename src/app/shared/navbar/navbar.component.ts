import {Component, OnInit, ElementRef} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SignupComponent} from '../../examples/signup/signup.component';
import {Route, Router} from '@angular/router';
import {SearchComponent} from '../../examples/search/search.component';
import {window} from "rxjs/operators";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    storage: Storage;

    constructor(public location: Location, private element: ElementRef, private router: Router) {
        this.sidebarVisible = false;
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.storage = localStorage;
    }
    
    logOut() {
        localStorage.removeItem('id');
        localStorage.removeItem('token');
        location.replace('/#/home')
    }

    onNone() {
        const obj = document.getElementById('search-nav');
        console.log(obj);
        if (this.location.path() === '/search-page') {
            obj.style.display = 'none';
        } else {
            obj.style.display = 'block';
        }
    }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];

        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };

    onEnter(inputElement: string) {
        SearchComponent.searchElement = inputElement;
        this.router.navigate(['/search-page']);
    }

    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };

    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    isHome() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/home') {
            return true;
        } else {
            return false;
        }
    }

    isMessages() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/messages') {
            return true;
        }
        // tslint:disable-next-line:one-line
        else {
            return false;
        }
    }

    isSearch() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/messages') {
            return true;
        }
        // tslint:disable-next-line:one-line
        else {
            return false;
        }
    }


    isDocumentation() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/documentation') {
            return true;
        } else {
            return false;
        }
    }

    isSearchNav() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/search-page') {
            return true;
        }
        // tslint:disable-next-line:one-line
        else {
            return false;
        }
    }
}
